#
# module: ast.py
#
#  This module contains the definitions of the abstract syntax
#  tree nodes.
#
#######################################
# Prog
#######################################
class Prog():
   '''
   Prog: Prog ([Decl], [Stmt])

   decls: list of declarations
   stmts: list of statments
   '''
   def __init__(self, decls, stmts):
      self.decls = decls
      self.stmts = stmts
   def __str__(self):
      decls_str = _listToString(self.decls)
      stmts_str = _listToString(self.stmts)
      return "Prog (" + decls_str + ", " + stmts_str + ")"

#######################################
# Declarations
#######################################
class Var():
   '''
   Decl: Var (string, [Expr], Type)

   name: variable name
   array_dims: list of array dimensions
   m_type: variable type
   '''
   def __init__(self, name, array_dims, m_type):
      self.name = name
      self.array_dims = array_dims
      self.m_type = m_type
   def __str__(self):
      array_dims_str = _arrayDimsToString(self.array_dims)
      return "Var (" + str(self.name) + "," + array_dims_str + "," + str(self.m_type) + ")" 

class Fun():
   '''
   Decl: Fun (string, [Parameter], Type, [decl], [stmt])

   name: function name
   params: list of function parameters
   m_type: function return type
   decls: list of declarations
   stmts: list of statements
   '''
   def __init__(self, name, params, m_type, decls, stmts):
      self.name = name
      self.params = params
      self.m_type = m_type
      self.decls = decls
      self.stmts = stmts
   def __str__(self):
      params_str = _listToString(self.params)
      decls_str = _listToString(self.decls)
      stmts_str = _listToString(self.stmts)
      return "Fun (" + str(self.name) + "," + params_str + "," + str(self.m_type) + "," + decls_str + "," + stmts_str + ")"

class Parameter():
   '''
   Parameter: (string, int, Type)

   name: parameter name
   arr_dim: array dimensions (int)
   m_type: parameter type
   '''
   def __init__(self, name, arr_dim, m_type):
      self.name = name
      self.arr_dim = arr_dim
      self.m_type = m_type
   def __str__(self):
      return "(" + str(self.name) + "," + str(self.arr_dim) + "," + str(self.m_type) + ")"

class Data():
   '''
   Decl: Data (string, [Constructor])

   name: data type name
   cons: list of constructors
   '''
   def __init__(self, name, cons):
      self.name = name
      self.cons = cons
   def __str__(self):
      cons_str = _listToString(self.cons)
      return "Data (" + str(self.name) + "," + cons_str + ")"

class Constructor():
   '''
   Constructor: (string, [Type])

   name: constructor name
   types: list of types
   '''
   def __init__(self, name, types):
      self.name = name
      self.types = types
   def __str__(self):
      types_str = _listToString(self.types)
      return "(" + str(self.name) + "," + types_str + ")"

#######################################
# Statements
#######################################
class Assign():
   '''
   Stmt: Assign (string, [Expr], Expr)

   name: var name
   array_dims: array dimensions
   expr: expression (RHS expression)
   '''
   def __init__(self, name, array_dims, expr, lineno):
      self.name = name
      self.array_dims = array_dims
      self.expr = expr
      self.lineno = lineno
   def __str__(self):
      arr_dims_str = _arrayDimsToString(self.array_dims)
      return "Assign (" + str(self.name) + "," + arr_dims_str + "," + str(self.expr) + ")"

class While():
   '''
   Stmt: While (Expr, Stmt)

   expr: while expression
   stmt: do statment
   '''
   def __init__(self, expr, stmt, lineno):
      self.expr = expr
      self.stmt = stmt
      self.lineno = lineno
   def __str__(self):
      return "While (" + str(self.expr) + "," + str(self.stmt) + ")"

class Cond():
   '''
   Stmt: Cond (Expr, Stmt, Stmt)

   expr: if condition
   then_stmt: then statement
   else_stmt: else statement
   '''
   def __init__(self, expr, then_stmt, else_stmt, lineno):
      self.expr = expr
      self.then_stmt = then_stmt
      self.else_stmt = else_stmt
      self.lineno = lineno
   def __str__(self):
      return "Cond (" + str(self.expr) + "," + str(self.then_stmt) + "," + str(self.else_stmt) + ")"

class Read():
   '''
   Stmt: Read (string, [Expr])

   var: var to read
   array_dims: array dimensions
   '''
   def __init__(self, var, array_dims, lineno):
      self.var = var
      self.array_dims = array_dims
      self.lineno = lineno
   def __str__(self):
      array_dims_str = _arrayDimsToString(self.array_dims)
      return "Read (" + str(self.var) + "," + array_dims_str + ")"

class Print():
   '''
   Stmt: Print (Expr)

   expr: print expression
   '''
   def __init__(self, expr, lineno):
      self.expr = expr
      self.lineno = lineno
   def __str__(self):
      return "Print (" + str(self.expr) + ")"

class Return():
   '''
   Stmt: Return (Expr)

   expr: return expression
   '''
   def __init__(self, expr, lineno):
      self.expr = expr
      self.lineno = lineno
   def __str__(self):
      return "Return (" + str(self.expr) + ")"

class Block():
   '''
   Stmt: Block ([Decl], [Stmt])

   decls: list of declarations
   stmts: list of stmts
   '''
   def __init__(self, decls, stmts):
      self.decls = decls
      self.stmts = stmts
   def __str__(self):
      decls_str = _listToString(self.decls)
      stmts_str = _listToString(self.stmts)
      return "Block (" + decls_str + "," + stmts_str + ")"

class Case():
   '''
   Stmt: Case (Expr, [CaseStmt])

   expr: case expressions
   case_stmts: a list of case statements
   '''
   def __init__(self, expr, case_stmts, lineno):
      self.expr = expr
      self.case_stmts = case_stmts
      self.lineno = lineno
   def __str__(self):
      case_stmts_str = _listToString(self.case_stmts)
      return "Case (" + str(self.expr) + "," + case_stmts_str + ")"

class CaseStmt():
   '''
   CaseStmt: (string, [string], Stmt)

   cid: the constructor name
   var_list: a list of variable names
   stmt: the case stmt
   '''
   def __init__(self, cid, var_list, stmt, lineno):
      self.cid = cid
      self.var_list = var_list
      self.stmt = stmt
      self.lineno = lineno
   def __str__(self):
      var_list_str = _listToString(self.var_list)
      return "(" + str(self.cid) + "," + var_list_str + "," + str(self.stmt) + ")"

#######################################
# Types
#######################################
class Type():
   '''
   Type: String

   m_type: the type as a string value
   '''
   def __init__(self, m_type):
      self.m_type = m_type
   def __str__(self):
      return str(self.m_type)

#######################################
# Expressions
#######################################
class IVal():
   '''
   Expr: IVal int

   val: the int value
   '''
   def __init__(self, val, lineno):
      self.val = val
      self.lineno = lineno
   def __str__(self):
      return "IVal " + str(self.val)

class RVal():
   '''
   Expr: RVal float

   val: the float value
   '''
   def __init__(self, val, lineno):
      self.val = val
      self.lineno = lineno
   def __str__(self):
      return "RVal " + str(self.val)

class BVal():
   '''
   Expr: BVal bool

   val: the boolean value
   '''
   def __init__(self, val, lineno):
      self.val = val
      self.lineno = lineno
   def __str__(self):
      return "BVal " + str(self.val)

class CVal():
   '''
   Expr: CVal char

   val: the char value
   '''
   def __init__(self, val, lineno):
      self.val = val
      self.lineno = lineno
   def __str__(self):
      return "CVal " + str(self.val)

class Size():
   '''
   Expr: Size (string, int)

   var: the variable name
   num_dims: number of array dimensions
   '''
   def __init__(self, var, val, lineno):
      self.var = var
      self.num_dims = val
      self.lineno = lineno
   def __str__(self):
      return "Size (" + str(self.var) + "," + str(self.num_dims) + ")"

class Id():
   '''
   Expr: Id (string, [Expr])

   var: the variable name
   array_dims: the array dimensions for the variable
   '''
   def __init__(self, var, array_dims, lineno):
      self.var = var
      self.array_dims = array_dims
      self.lineno = lineno
   def __str__(self):
      array_dims_str = _arrayDimsToString(self.array_dims)
      return "Id (" + str(self.var) + "," + array_dims_str + ")"

class App():
   '''
   Expr: App (Op, [Expr])

   op: the operation 
   exprs: a list of expressions
   '''
   def __init__(self, op, exprs, lineno):
      self.op = op
      self.exprs = exprs
      self.lineno = lineno
   def __str__(self):
      exprs_str = _listToString(self.exprs)
      return "App (" + str(self.op) + "," + exprs_str + ")"

#######################################
# Operations
#######################################
class Fn():
   '''
   Op: Fn string

   name: the function name
   '''
   def __init__(self, name):
      self.name = name
   def __str__(self):
      return "Fn " + str(self.name)

class CId():
   '''
   Op: CId string

   name: the constructor name
   '''
   def __init__(self, name):
      self.name = name
   def __str__(self):
      return "Cid (" + str(self.name) + ")"

class Op():
   '''
   Op: Op

   op: a string representation of the operation
   '''
   def __init__(self, op):
      self.op = op
   def __str__(self):
      return str(self.op)

#######################################
# Helper Functions
#######################################
def _listToString(m_list):
   ret = "[]"
   if len(m_list) > 0:
      ret = "["
      for item in m_list[:-1]:
         ret += str(item) + ","
      ret += str(m_list[-1]) + "]"

   return ret

def _arrayDimsToString(array_dims):
   ret = "[]"
   if len(array_dims) > 0:
      ret = ""
      for d in array_dims:
         ret += "[" + str(d) + "]"

   return ret
 
