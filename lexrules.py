#
# module: lexrules.py
#
#  This module contains all of the lexer rules used by lex.py.
#  When an error occurs, the LexError exception is raised which
#  is defined in errors.py
#
from errors import LexError

# Global variables
inMultilineCommentState = False     # This variable is used to track if the lexer is in the multiline comment state

# Lexer states
states = (
   ('MULTILINECOMMENT', 'exclusive'),
)

# Lexer tokens
tokens = [
   'ADD',
   'SUB',
   'MUL',
   'DIV',
   'ARROW',
   'AND',
   'OR',
   'EQUAL',
   'LT',
   'GT',
   'LE',
   'GE',
   'ASSIGN',
   'LPAR',
   'RPAR',
   'CLPAR',
   'CRPAR',
   'SLPAR',
   'SRPAR',
   'SLASH',
   'COLON',
   'SEMICOLON',
   'COMMA',
   'CID',
   'ID',
   'RVAL',
   'IVAL',
   'BVAL',
   'CVAL',
]

reserved = {
   'not'    : 'NOT',
   'if'     : 'IF',
   'then'   : 'THEN',
   'while'  : 'WHILE',
   'do'     : 'DO',
   'read'   : 'READ',
   'else'   : 'ELSE',
   'begin'  : 'BEGIN',
   'end'    : 'END',
   'case'   : 'CASE',
   'of'     : 'OF',
   'print'  : 'PRINT',
   'int'    : 'INT',
   'bool'   : 'BOOL',
   'char'   : 'CHAR',
   'real'   : 'REAL',
   'var'    : 'VAR',
   'data'   : 'DATA',
   'size'   : 'SIZE',
   'float'  : 'FLOAT',
   'floor'  : 'FLOOR',
   'ceil'   : 'CEIL',
   'fun'    : 'FUN',
   'return' : 'RETURN',
}

tokens = tokens + list(reserved.values())

# Simple token matchers
t_ASSIGN = r':='
t_LE     = r'=<'
t_GE     = r'>='
t_AND    = r'&&'
t_OR     = r'\|\|'
t_ARROW  = r'=>'

t_ADD    = r'\+'
t_SUB    = r'-'
t_MUL    = r'\*'
t_DIV    = r'/'
t_EQUAL  = r'='
t_LT     = r'<'
t_GT     = r'>'
t_LPAR   = r'\('
t_RPAR   = r'\)'
t_CLPAR  = r'{'
t_CRPAR  = r'}'
t_SLPAR  = r'\['
t_SRPAR  = r'\]'
t_SLASH  = r'\|'
t_COLON  = r':'
t_SEMICOLON = r';'
t_COMMA  = r','

# More complicated token matchers
def t_CID(t):
   r'\#([a-zA-Z0-9_]*)'
   return t

def t_BVAL(t):
   r'(true|false)'
   return t

def t_ID(t):
   r'[a-zA-Z]([a-zA-Z0-9_]*)'
   t.type = reserved.get(t.value, 'ID')
   return t

def t_RVAL(t):
   r'([0-9]+)\.([0-9]+)'
   return t

def t_IVAL(t):
   r'([0-9]+)'
   return t

def t_CVAL(t):
   r'"([a-zA-Z0-9_^ ]|\\n)"'
   return t

def t_ANY_comment(t):
   r'%.*'
   pass

def t_multilineComment(t):
   r'/\*'
   t.lexer.comment_start = t.lexer.lexpos
   t.lexer.comment_lines = 0
   t.lexer.level = 1
   t.lexer.begin('MULTILINECOMMENT')

def t_newline(t):
   r'\n+'
   t.lexer.lineno += len(t.value)

t_ignore = ' \t'

def t_error(t):
   raise LexError("Lex Error [" + str(t.lexer.lineno) + "]: Unknown token '" + str(t.value[0]) + "'")

#########################################
# Multiline comment state
#########################################
def t_MULTILINECOMMENT_multilineComment(t):
   r'/\*'
   global inMultilineCommentState
   inMultilineCommentState = True
   t.lexer.level += 1

def t_MULTILINECOMMENT_end(t):
   r'\*/'
   global inMultilineCommentState

   t.lexer.level -= 1
   if t.lexer.level == 0:
      t.value = t.lexer.lexdata[t.lexer.comment_start:t.lexer.lexpos+1]
      t.lexer.lineno += t.lexer.comment_lines
      inMultilineCommentState = False
      t.lexer.begin('INITIAL')

def t_MULTILINECOMMENT_newline(t):
   r'\n+'
   t.lexer.comment_lines += len(t.value)

t_MULTILINECOMMENT_ignore = ' \t'

def t_MULTILINECOMMENT_error(t):
   t.lexer.skip(1)

