##CPSC 411 - M++ Compiler (Assignment 3, 4, 5)
Ian Macaulay

This compiler is used on the M Laguage specified in 
http://pages.cpsc.ucalgary.ca/~robin/class/411/M+/Mspec.pdf.
It has been built using PLY (http://www.dabeaz.com/ply). The compiler
supports all functionality of M++ (All basic functionality, arrays, and 
constructors)
See *Things that dont work properly* below for what does not work

###Dependencies
- Python 2.7

###3rd Party Tools
- PLY 3.4 (see ply folder)

###Usage
`./mc.py [filePath]`

ex. `./mc.py testFiles/code_test1.m`

###Program Structure
The compiler for the M language is built using lex and yacc provided by PLY.
yacc generates two files: *parser.out* and *parsetab.py*
The code is structured in the following way:

- Reading the file, creating the lexer, and starting the parser is done in `mc.py`
- The lex rules are defined in `lexrules.py`
- The grammar rules are defined in `parserules.py`
- Errors are defined in `errors.py`
- The abstract syntax tree is defined in `ast.py`
- The symbol table is defined in `symboltable.py`
- The semantic checking is done in `semantics.py`
- The intermediate representation is defined in `ir.py`
- The intermediate representation is generated in `irgenerator.py`
- The code generation is done in `codegenerator.py`

###Test Files
Test files can be found in the *testFiles* directory.

- *lexer_test.m* was used to test the lex rules before the parser was built
- File names of the form *parser_test_error\*.m* are produce syntax and lex errors
- File names of the form *parser_test\*.m* are valid test cases for the parser
- File names of the form *semantic_test\*.m* are valid test cases for the semantic checking
- File names of the form *semantic_test_error\*.m* produce the semantic error messages
- File names of the form *code_test\*.m* are valid test cases to generate the stack machine code
- Files in *testFiles/m_tests* directory are test cases found on Dr. Cocketts website

###Output
The program will output a message for each stage of compilation
If an error occurs:

- The error message will be outputted in the form `[Error Type] [Line No.]: [Message]`
    
###Things that dont work properly
- The only feature not implemented from the spec is the extended syntax for functions
(ie. `fun MAX(n,m:int):int;`)
- Other than that, all input seems to work and line numbers seem to be tracked correctly
