#
# module: irgenerator.py
#
from ir import *
from ast import *
from symboltable import *

class IRGenerator():
   def __init__(self):
      self._symboltable = SymbolTable()

   def generate(self, ast):
      '''
      Generates an ir from the given ast
      '''
      self._symboltable.new_scope(ProgScope())

      (num_local_vars, functions, array_descs) = self._process_decls(ast.decls)
      body_stmts = self._process_stmts(ast.stmts)

      self._symboltable.remove_scope()

      return I_Prog(functions, num_local_vars, array_descs, body_stmts)

#################################################
# Declaration functions
#################################################
   def _process_decls(self, decls):
      '''
      Processes the declarations given in decls
      '''
      num_local_vars = 0
      functions = []
      array_descs = []

      for decl in decls:
         if isinstance(decl, Var):
            self._add_var_decl_to_st(decl)
         elif isinstance(decl, Fun):
            self._add_fun_decl_to_st(decl)
         elif isinstance(decl, Data):
            self._add_data_decl_to_st(decl)

      for decl in decls:
         if isinstance(decl, Var):
            array_desc = self._process_var_decl(decl)
            if array_desc:
               array_descs.append(array_desc)
            num_local_vars += 1
         elif isinstance(decl, Fun):
            fun = self._process_function_decl(decl)
            functions.append(fun)
         elif isinstance(decl, Data):
            self._process_data_decl(decl)

      return (num_local_vars, functions, array_descs)

   def _process_var_decl(self, var_decl):
      '''
      Generates the I_ArrDesc from the var_decl
      '''
      # process the array expressions
      array_exprs = []
      for e in var_decl.array_dims:
         expr = self._process_expr(e)
         array_exprs.append(expr)
      
      if len(array_exprs) == 0:
         return None
      
      var_desc = self._symboltable.lookup(var_decl.name)
      return I_ArrDesc(var_desc.offset, array_exprs)

   def _process_function_decl(self, fun_decl):
      '''
      Generates the I_Fun from the fun_decl
      '''
      # process the function scope
      self._symboltable.new_scope(FunScope(fun_decl.m_type))
      for param in reversed(fun_decl.params):
         self._add_fun_arg_to_st(param)

      (num_local_vars, functions, array_descs) = self._process_decls(fun_decl.decls)
      body_stmts = self._process_stmts(fun_decl.stmts)
      self._symboltable.remove_scope()

      fun_desc = self._symboltable.lookup(fun_decl.name)

      return I_Fun(fun_desc.label, functions, num_local_vars, len(fun_decl.params), array_descs, body_stmts)

   def _process_data_decl(self, data_decl):
      '''
      Processes the data_decl
      '''
      pass

   def _add_var_decl_to_st(self, var_decl):
      '''
      Adds a variable to the symbol table
      '''
      self._symboltable.insert(Variable(var_decl.name, var_decl.m_type, len(var_decl.array_dims)))

   def _add_fun_decl_to_st(self, fun_decl):
      '''
      Adds a function to the symbol table
      '''
      arg_types = []
      for param in fun_decl.params:
         arg_types.append((param.m_type, param.arr_dim))
      self._symboltable.insert(Function(fun_decl.name, arg_types, fun_decl.m_type))

   def _add_fun_arg_to_st(self, fun_arg):
      '''
      Adds a function argument to the symbol table
      '''
      self._symboltable.insert(Argument(fun_arg.name, fun_arg.m_type, fun_arg.arr_dim))

   def _add_data_decl_to_st(self, data_decl):
      '''
      Adds a user defined type to the symbol table, then adds all of its constructors
      '''
      # add user type
      self._symboltable.insert(DataType(data_decl.name))
      # add constructors
      for con in data_decl.cons:
         self._symboltable.insert(Constructor(con.name, con.types, data_decl.name))

#################################################
# Statment functions
#################################################
   def _process_stmts(self, stmts):
      '''
      Processes each stmt in stmts
      '''
      stmt_list = []
      for s in stmts:
         stmt = self._process_stmt(s)
         stmt_list.append(stmt)

      return stmt_list

   def _process_stmt(self, stmt):
      '''
      Processes the stmt based on the type of stmt
      '''
      if isinstance(stmt, Assign):
         return self._process_assign(stmt)
      elif isinstance(stmt, While):
         return self._process_while(stmt)
      elif isinstance(stmt, Cond):
         return self._process_cond(stmt)
      elif isinstance(stmt, Case):
         return self._process_case(stmt)
      elif isinstance(stmt, Read):
         return self._process_read(stmt)
      elif isinstance(stmt, Print):
         return self._process_print(stmt)
      elif isinstance(stmt, Return):
         return self._process_return(stmt)
      elif isinstance(stmt, Block):
         return self._process_block(stmt)

   def _process_assign(self, stmt):
      '''
      Generates the I_Assign from the stmt
      '''
      var_desc = self._symboltable.lookup(stmt.name)

      array_indicies = []
      for e in stmt.array_dims:
         expr = self._process_expr(e)
         array_indicies.append(expr)

      expr = self._process_expr(stmt.expr)
      
      return I_Assign(var_desc.level, var_desc.offset, array_indicies, expr)

   def _process_while(self, stmt):
      '''
      Generates the I_While from the stmt
      '''
      expr = self._process_expr(stmt.expr)
      do_stmt = self._process_stmt(stmt.stmt)

      return I_While(expr, do_stmt)

   def _process_cond(self, stmt):
      '''
      Generates the I_Cond from the stmt
      '''
      expr = self._process_expr(stmt.expr)
      then_stmt = self._process_stmt(stmt.then_stmt)
      else_stmt = self._process_stmt(stmt.else_stmt)

      return I_Cond(expr, then_stmt, else_stmt)

   def _process_case(self, stmt):
      '''
      Generates the I_Case from the stmt
      '''
      expr = self._process_expr(stmt.expr)
      case_stmts = []
      for cs in stmt.case_stmts:
         constructor = self._symboltable.lookup(cs.cid)
         self._symboltable.new_scope(CaseScope())
         for (var, t) in zip(cs.var_list, constructor.arg_types):
            self._symboltable.insert(Variable(var, t, 0))
         case_stmt = self._process_stmt(cs.stmt)
         self._symboltable.remove_scope()
         case_stmts.append(I_CaseStmt(constructor.cons_num, len(constructor.arg_types), case_stmt))

      return I_Case(expr, case_stmts)

   def _process_read(self, stmt):
      '''
      Generates the correct I_Read from the stmt based on the type
      '''
      var_desc = self._symboltable.lookup(stmt.var)
      array_indicies = []
      for e in stmt.array_dims:
         expr = self._process_expr(e)
         array_indicies.append(expr)

      if str(var_desc.m_type) == "int":
         return I_ReadInt(var_desc.level, var_desc.offset, array_indicies)
      elif str(var_desc.m_type) == "real":
         return I_ReadFloat(var_desc.level, var_desc.offset, array_indicies)
      elif str(var_desc.m_type) == "bool":
         return I_ReadBool(var_desc.level, var_desc.offset, array_indicies)
      elif str(var_desc.m_type) == "char":
         return I_ReadChar(var_desc.level, var_desc.offset, array_indicies)

   def _process_print(self, stmt):
      '''
      Generates the correct I_Print from the stmt based on the type
      '''
      expr = self._process_expr(stmt.expr)

      if isinstance(expr, I_IVal):
         return I_PrintInt(expr)
      elif isinstance(expr, I_RVal):
         return I_PrintFloat(expr)
      elif isinstance(expr, I_BVal):
         return I_PrintBool(expr)
      elif isinstance(expr, I_CVal):
         return I_PrintChar(expr)
      elif isinstance(expr, I_Size):
         return I_PrintInt(expr)
      elif isinstance(expr, I_Id):
         var_desc = self._symboltable.lookup(stmt.expr.var)
         if self._types_match(var_desc.m_type, "int"):
            return I_PrintInt(expr)
         elif self._types_match(var_desc.m_type, "real"):
            return I_PrintFloat(expr)
         elif self._types_match(var_desc.m_type, "bool"):
            return I_PrintBool(expr)
         elif self._types_match(var_desc.m_type, "char"):
            return I_PrintChar(expr)
      elif isinstance(expr, I_App):
         if isinstance(expr.op, I_Call):
            fun_desc = self._symboltable.lookup(stmt.expr.op.name)
            if self._types_match(fun_desc.return_type, "int"):
               return I_PrintInt(expr)
            elif self._types_match(fun_desc.return_type, "real"):
               return I_PrintFloat(expr)
            elif self._types_match(fun_desc.return_type, "bool"):
               return I_PrintBool(expr)
            elif self._types_match(fun_desc.return_type, "char"):
               return I_PrintChar(expr)
         elif isinstance(expr.op, I_Op):
            if self._is_i_int_operation(expr.op.op):
               return I_PrintInt(expr)
            elif self._is_i_float_operation(expr.op.op):
               return I_PrintFloat(expr)
            elif self._is_i_boolean_operation(expr.op.op):
               return I_PrintBool(expr)
            elif self._is_i_comparision_operation(expr.op.op):
               return I_PrintBool(expr)
            elif self._is_i_builtin_operation(expr.op.op):
               return I_PrintFloat(expr)
            
   def _process_return(self, stmt):
      '''
      Generates an I_Return
      '''
      expr = self._process_expr(stmt.expr)
      return I_Return(expr)

   def _process_block(self, stmt):
      '''
      Generates an I_Block from the stmt
      '''
      self._symboltable.new_scope(BlockScope())

      (num_local_vars, functions, array_descs) = self._process_decls(stmt.decls)
      body_stmts = self._process_stmts(stmt.stmts)

      self._symboltable.remove_scope()

      return I_Block(functions, num_local_vars, array_descs, body_stmts)

#################################################
# Expression functions
#################################################
   def _process_expr(self, expr, is_func_arg=False):
      '''
      Generates the correct I_Expr based on the type of expr
      '''
      if isinstance(expr, IVal):
         return I_IVal(expr.val)
      elif isinstance(expr, RVal):
         return I_RVal(expr.val)
      elif isinstance(expr, BVal):
         return I_BVal(expr.val)
      elif isinstance(expr, CVal):
         return I_CVal(expr.val)
      elif isinstance(expr, Id):
         return self._process_id(expr, is_func_arg)
      elif isinstance(expr, App):
         return self._process_app(expr)
      elif isinstance(expr, Size):
         return self._process_size(expr)

   def _process_id(self, expr, is_func_arg=False):
      '''
      Generates an I_Id
      '''
      var_desc = self._symboltable.lookup(expr.var)
      if is_func_arg and var_desc.array_dims > 0:
         return I_Ref(var_desc.level, var_desc.offset)
      
      array_indicies = []
      for e in expr.array_dims:
         expr = self._process_expr(e)
         array_indicies.append(expr)
      return I_Id(var_desc.level, var_desc.offset, array_indicies)

   def _process_app(self, expr):
      '''
      Generates an I_App
      '''
      op = self._process_operation(expr.op, expr.exprs)
      exprs = []
      for e in expr.exprs:
         exp = None
         if isinstance(op, I_Call):
            exp = self._process_expr(e, True)
         else:
            exp = self._process_expr(e)

         exprs.append(exp)

      return I_App(op, exprs)

   def _process_size(self, expr):
      '''
      Generates an I_Size
      '''
      var_desc = self._symboltable.lookup(expr.var)
      return I_Size(var_desc.level, var_desc.offset, expr.num_dims)

   def _process_operation(self, op, exprs):
      '''
      Generates the correct I_Op based on the op
      '''
      if isinstance(op, Fn):
         fun_desc = self._symboltable.lookup(op.name)
         return I_Call(fun_desc.label, fun_desc.level)
      elif isinstance(op, CId):
         cons_desc = self._symboltable.lookup(op.name)
         return I_Cons(cons_desc.cons_num, len(cons_desc.arg_types))
      elif isinstance(op, Op):
         if self._is_arithmetic_op(op.op):
            return self._i_arithmetic_op_for_op(op.op, exprs)
         elif self._is_comparison_op(op.op):
            return self._i_comparision_op_for_op(op.op, exprs)
         elif self._is_boolean_op(op.op):
            return I_Op("I_" + str(op.op))
         elif self._is_builtin_op(op.op):
            return I_Op("I_" + str(op.op))

#################################################
# Helper functions
#################################################
   def _types_match(self, actual, expected):
      '''
      Returns true if the types match
      '''
      return str(actual) == str(expected)

   def _is_arithmetic_op(self, op):
      '''
      Returns true if the operation is an arithmetic operation
      '''
      return self._types_match(op, "Add") or self._types_match(op, "Sub") or self._types_match(op, "Mul") or self._types_match(op, "Div") or self._types_match(op, "Neg")

   def _is_comparison_op(self, op):
      '''
      Returns true if the operation is a comparison operation
      '''
      return self._types_match(op, "Lt") or self._types_match(op, "Le") or self._types_match(op, "Gt") or self._types_match(op, "Ge") or self._types_match(op, "Eq")

   def _is_boolean_op(self, op):
      ''' 
      Returns true if the operation is a boolean operation
      '''
      return self._types_match(op, "Not") or self._types_match(op, "And") or self._types_match(op, "Or")

   def _is_builtin_op(self, op):
      '''
      Returns true if the function is a built in function name
      '''
      return self._types_match(op, "Float") or self._types_match(op, "Floor") or self._types_match(op, "Ceil")

   def _is_i_int_operation(self, op):
      '''
      Returns true if the op is a intermediate arithmetic op for ints
      '''
      return self._types_match(op, "I_Add_Int") or self._types_match(op, "I_Mul_Int") or self._types_match(op, "I_Sub_Int") or self._types_match(op, "I_Div_Int") or self._types_match(op, "I_Neg_Int") 
            
   def _is_i_float_operation(self, op):
      '''
      Returns true if the op is an intermediate arithmetic op for floats
      '''
      return self._types_match(op, "I_Add_Float") or self._types_match(op, "I_Mul_Float") or self._types_match(op, "I_Sub_Float") or self._types_match(op, "I_Div_Float") or self._types_match(op, "I_Neg_Float") 

   def _is_i_boolean_operation(self, op):
      '''
      Returns true if the op is an intermediate boolean op
      '''
      return self._types_match(op, "I_Not") or self._types_match(op, "I_And") or self._types_match(op, "I_Or")

   def _is_i_comparision_operation(self, op):
      '''
      Returns true if the intermediate op is a comparison op
      '''
      isIntOp = self._types_match(op, "I_Lt_Int") or self._types_match(op, "I_Le_Int") or self._types_match(op, "I_Gt_Int") or self._types_match(op, "I_Ge_Int") or self._types_match(op, "I_Eq_Int") 
      isFloatOp = self._types_match(op, "I_Lt_Float") or self._types_match(op, "I_Le_Float") or self._types_match(op, "I_Gt_Float") or self._types_match(op, "I_Ge_Float") or self._types_match(op, "I_Eq_Float")
      isCharOp = self._types_match(op, "I_Lt_Char") or self._types_match(op, "I_Le_Char") or self._types_match(op, "I_Gt_Char") or self._types_match(op, "I_Ge_Char") or self._types_match(op, "I_Eq_Char")  
      return isIntOp or isFloatOp or isCharOp

   def _is_i_builtin_operation(self, op):
      '''
      Returns true if the op is a intermediate builtin op
      '''
      return self._types_match(op, "I_Float") or self._types_match(op, "I_Floor") or self._types_match(op, "I_Ceil")

   def _i_arithmetic_op_for_op(self, op, exprs):
      '''
      Converts an op to the correct intermediate arithmetic op based on expr types
      '''
      expr_type = self._type_of_exprs(exprs)
      if self._types_match(expr_type, "int"):
         return I_Op("I_" + str(op) + "_Int")
      elif self._types_match(expr_type, "real"):
         return I_Op("I_" + str(op) + "_Float")

   def _i_comparision_op_for_op(self, op, exprs):
      '''
      Converts an op to the correct intermediate comparision op based on expr types
      '''
      expr_type = self._type_of_exprs(exprs)
      if self._types_match(expr_type, "int"):
         return I_Op("I_" + str(op) + "_Int")
      elif self._types_match(expr_type, "real"):
         return I_Op("I_" + str(op) + "_Float")
      elif self._types_match(expr_type, "char"):
         return I_Op("I_" + str(op) + "_Char")

   def _type_of_exprs(self, exprs):
      '''
      Returns the type used in the expression list given
      '''
      for expr in exprs:
         if isinstance(expr, IVal):
            return "int"
         elif isinstance(expr, RVal):
            return "real"
         elif isinstance(expr, BVal):
            return "bool"
         elif isinstance(expr, CVal):
            return "char"
         elif isinstance(expr, Id):
            var_desc = self._symboltable.lookup(expr.var)
            return var_desc.m_type
         elif isinstance(expr, App):
            if isinstance(expr.op, Fn):
               fun_desc = self._symboltable.lookup(expr.op.name)
               return fun_desc.return_type
            elif isinstance(expr.op, Op):
               if self._is_comparison_op(expr.op.op):
                  return "bool"
               elif self._is_boolean_op(expr.op.op):
                  return "bool"
               elif self._is_builtin_op(expr.op.op):
                  return "real"
         elif isinstance(expr, Size):
            return "real"
