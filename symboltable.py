#
# module: symboltable.py
#
from errors import *

function_number = 1
constructor_number = 1

########################################################
# Symbol Table
########################################################
class SymbolTable():
   def __init__(self):
      global function_number, constructor_number
      function_number = 1
      constructor_number = 1
      self.scope_levels = []
   def __str__(self):
      if len(self.scope_levels) == 0:
         return "[]"

      ret = ""
      for scope in self.scope_levels:
         ret += str(scope) + "\n"
      return ret
   def __len__(self):
      return len(self.scope_levels)

   def new_scope(self, level_type):
      '''
      Creates a new scope level of type level_type

      level_type: the type of scope level to create
      '''
      level = len(self.scope_levels)
      self.scope_levels.insert(0, _ScopeLevel(level_type, level))

   def lookup(self, symbol):
      '''
      Looks up the symbol in the symbol table. Returns an ISymbolDesc object

      symbol: the symbol to look up in the symbol table
      '''
      # check each scope level for the symbol
      for scope in self.scope_levels:
         symbol_i_desc = scope.lookup(symbol)
         if symbol_i_desc:
            return symbol_i_desc

      raise SymbolNotFoundError(symbol)

   def insert(self, symbol_desc):
      '''
      Inserts a new symbol into the top most scope level of the symbol table

      symbol_desc: the symbol decscription object to insert
      '''
      if len(self.scope_levels) == 0:
         raise NoScopeDefinedError()

      self.scope_levels[0].insert(symbol_desc)

   def remove_scope(self):
      '''
      Removes the top most scope level from the symbol table
      '''
      self.scope_levels = self.scope_levels[1:]

   def return_type(self):
      '''
      Returns the return type of the top scope level if it is a function
      '''
      if len(self.scope_levels) == 0:
         raise NoScopeDefinedError()

      level_type = self.scope_levels[0].level_type
      if isinstance(level_type, FunScope):
         return level_type.return_type
      else:
         raise ReturnFromNonFunctionError()

########################################################
# SymbolDesc
#    Objects that can be inserted into the symbol table
########################################################
class Argument():
   '''
   name: argument name
   m_type: argument type
   array_dims: number of array dimenstions
   '''
   def __init__(self, name, m_type, array_dims):
      self.name = name
      self.m_type = m_type
      self.array_dims = array_dims

class Variable():
   '''
   name: variable name
   m_type: variable type
   array_dims: number of array array_dims
   '''
   def __init__(self, name, m_type, array_dims):
      self.name = name
      self.m_type = m_type
      self.array_dims = array_dims

class Function():
   '''
   name: function name
   arg_types [(type, int)]: the argument types where type is the type and int is the number of array dims
   return_type: the return type
   '''
   def __init__(self, name, arg_types, return_type):
      self.name = name
      self.arg_types = arg_types
      self.return_type = return_type

class DataType():
   '''
   name: the user defined type
   '''
   def __init__(self, name):
      self.name = name

class Constructor():
   '''
   name: the constructor name
   arg_types [type]: a list of argument types
   user_type: the user defined type for this constructor
   ''' 
   def __init__(self, name, arg_types, user_type):
      self.name = name
      self.arg_types = arg_types
      self.user_type = user_type

########################################################
# ISymbolDesc
#    Objects returned by the symbol table lookup
########################################################
class IVariable():
   '''
   level: the scope level
   offset: the variable offset
   m_type: the variable type
   array_dims: the number of array dimenstions
   '''
   def __init__(self, level, offset, m_type, array_dims):
      self.level = level
      self.offset = offset
      self.m_type = m_type
      self.array_dims = array_dims
   def __str__(self):
      return "IVariable"

class IFunction():
   '''
   level: the scope level
   label: the function label
   arg_types [(type, int)]: a list of argument types where type is the type and int is the array dims
   return_type: the function return type 
   '''
   def __init__(self, level, label, arg_types, return_type):
      self.level = level
      self.label = label
      self.arg_types = arg_types
      self.return_type = return_type
   def __str__(self):
      return "IFunction"

class IConstructor():
   '''
   cons_num: the internal constructor number
   arg_types [type]: a list of argument types
   user_type: the user defined type for the constructor
   '''
   def __init__(self, cons_num, arg_types, user_type):
      self.cons_num = cons_num
      self.arg_types = arg_types
      self.user_type = user_type
   def __str__(self):
      return "IConstructor"

class IType():
   '''
   cons_names []: a list of constructor names for the user type
   '''
   def __init__(self, cons_names):
      self.cons_names = cons_names
   def __str__(self):
      return "IType"

########################################################
# Scope Level
#    Represents a scope level in the symbol table
########################################################
class _ScopeLevel():
   def __init__(self, level_type, level):
      self.level_type = level_type
      self._level = level
      self._num_locals = 0
      self._num_args = 0
      self._symbol_values = []
      self._arg_offset = -4
      self._var_offset = 1
   def __str__(self):
      ret = "[" + str(self.level_type) + " " + str(self._level) + ": " + str(self._num_locals) + " " + str(self._num_args) + "\n"
      if len(self._symbol_values) > 0:
         for (n, v) in self._symbol_values[:-1]:
            ret += "\t(" + str(n) + ", " + str(v) + ")\n"
         (n, v) = self._symbol_values[-1]
         ret += "\t(" + str(n) + ", " + str(v) + ")]"
      else:
         ret += "]"
      return ret

   def lookup(self, symbol):
      '''
      Looks up the symbol in the symbol table. Returns an IDesc object if found
      '''
      for (name, attr) in self._symbol_values:
         # symbol found
         if name == symbol:
            return self._convert_to_i_desc(attr)

      return None

   def insert(self, symbol_desc):
      '''
      Inserts a new symbol in the symboltable if it does already exist
      '''
      if self.lookup(symbol_desc.name):
         raise SymbolRedefinedError(symbol_desc.name)

      if isinstance(symbol_desc, Argument):
         self._insert_argument(symbol_desc)
      elif isinstance(symbol_desc, Variable):
         self._insert_variable(symbol_desc)
      elif isinstance(symbol_desc, Function):
         self._insert_function(symbol_desc)
      elif isinstance(symbol_desc, DataType):
         self._insert_datatype(symbol_desc)
      elif isinstance(symbol_desc, Constructor):
         self._insert_constructor(symbol_desc)

   def _convert_to_i_desc(self, attr):
      '''
      Converts a Desc object to a IDesc object
      '''
      if isinstance(attr, _VarAttr):
         return IVariable(self._level, attr.offset, attr.m_type, attr.array_dims)
      elif isinstance(attr, _FunAttr):
         return IFunction(self._level, attr.label, attr.arg_types, attr.return_type)
      elif isinstance(attr, _ConsAttr):
         return IConstructor(attr.cons_num, attr.arg_types, attr.user_type)
      elif isinstance(attr, _TypeAttr):
         return IType(attr.cons_names)

   def _insert_argument(self, argument):
      '''
      Inserts an Argument into the symbol table
      '''
      attr = _VarAttr(self._arg_offset, argument.m_type, argument.array_dims)
      # arguments need to go in reverse order so always put them at the front
      self._symbol_values.insert(0, (argument.name, attr))
      self._arg_offset -= 1
      self._num_args += 1

   def _insert_variable(self, variable):
      '''
      Inserts a Variable into the symbol table
      '''
      attr = _VarAttr(self._var_offset, variable.m_type, variable.array_dims)
      self._symbol_values.append((variable.name, attr))
      self._var_offset += 1
      self._num_locals += 1

   def _insert_function(self, function):
      '''
      Inserts a function into the symboltable
      '''
      global function_number
      label = "func_label_" + str(function_number)
      attr = _FunAttr(label, function.arg_types, function.return_type)
      self._symbol_values.append((function.name, attr))
      function_number += 1

   def _insert_datatype(self, datatype):
      '''
      Inserts a DataType into the symbol table
      '''
      attr = _TypeAttr()
      self._symbol_values.append((datatype.name, attr))

   def _insert_constructor(self, constructor):
      '''
      Inserts a Constructor into the symbol table if the user type has been defined
      '''
      global constructor_number
      datatype = self.lookup(constructor.user_type)
      if not datatype:
         raise UserDefinedTypeNotFoundError(constructor.user_type)

      attr = _ConsAttr(constructor_number, constructor.arg_types, constructor.user_type)
      self._symbol_values.append((constructor.name, attr))
      constructor_number += 1
      datatype.cons_names.append(constructor.name)

class ProgScope():
   def __init__(self):
      self.scope_type = "Prog"

class FunScope():
   def __init__(self, return_type):
      self.scope_type = "Fun"
      self.return_type = return_type

class BlockScope():
   def __init__(self):
      self.scope_type = "Block"

class CaseScope():
   def __init__(self):
      self.scope_type = "Case"

########################################################
# SymbolAttr
#    Objects that are stored in a scope level
########################################################
class _VarAttr():
   def __init__(self, offset, m_type, array_dims):
      self.offset = offset
      self.m_type = m_type
      self.array_dims = array_dims
   def __str__(self):
      return "VarAttr(" + str(self.offset) + ", " + str(self.m_type) + ", " + str(self.array_dims) + ")"

class _FunAttr():
   def __init__(self, label, arg_types, return_type):
      self.label = label
      self.arg_types = arg_types
      self.return_type = return_type
   def __str__(self):
      arg_type_str = "["
      for (t, i) in self.arg_types:
         arg_type_str += "(" + str(t) + ", " + str(i) + ")"
      arg_type_str += "]"
      return "FunAttr(" + str(self.label) + ", " + arg_type_str + ", " + str(self.return_type) + ")"

class _ConsAttr():
   def __init__(self, cons_num, arg_types, user_type):
      self.cons_num = cons_num
      self.arg_types = arg_types
      self.user_type = user_type
   def __str__(self):
      arg_types_str = "["
      if len(self.arg_types) > 0:
         for a in self.arg_types[:-1]:
            arg_types_str += str(a) + ", "
         arg_types_str += str(self.arg_types[-1])
      arg_types_str += "]"
      return "ConsAttr(" + str(self.cons_num) + ", " + arg_types_str + ", " + str(self.user_type) + ")"

class _TypeAttr():
   def __init__(self):
      self.cons_names = []
   def __str__(self):
      cons_names_str = "["
      for c in self.cons_names[:-1]:
         cons_names_str += str(c) + ", "
      cons_names_str += str(self.cons_names[-1]) + "]"
      return "TypeAttr(" + cons_names_str + ")"