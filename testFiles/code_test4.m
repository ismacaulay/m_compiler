/*
 prints all the decimals values between the given number and the next number
 and prints the number of decimals
*/
var x:real;

fun decimal(x:real):int
{
   var b:bool;
   var c:int;
   var max_x:real;
   max_x := float(ceil(x));
   b := true;
   c := 0;
   while b do
   {
      print x;
      x := x + 0.1;
      if x >= max_x
         then
            b := false
         else
            c := c + 1;
   };
   return c;
};

read x;
print decimal(x);
