+
-
*
/
=>
&&
||
not
=
>
<
=<
>=
:=
(
)
{
}
[
]
|
:
;
,
if
then
while
do
read
else
begin
end
case
of
print
int
bool
char
real
var
data
size
float
floor
ceil
fun
return

#a
#_ab
#_123
#1_23
a_b
a__03b
a
0
1
2
0.1
0.2
1.2
false
true
"a"
"_"
"^"
"\t"
"\n"
% this is a comment

/*
this is a multi line comment
*/

/*
/* this is a nested commend
% and this
*/
% this too
*/

