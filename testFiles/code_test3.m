/*
Checks that constructors and case statements work correctly
*/
var y:intlist;
var b:bool;
var v:int;

data intlist = #nil | #cons of int * intlist;

y := #cons(6, #cons(3, #cons(42, #nil)));
b := true;

while b do
{
   case y of {
      #nil => b := false
      | #cons(h, t) => { 
         case t of {
            #nil => { v:= h; y:= #nil; }
            | #cons(th, tt) => { v:= th; y := tt; }
         };
      }
   };
};

print v;
