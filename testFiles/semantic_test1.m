var x[2]:int;
var x1 : int;
var y:real;
var y1:real;
var z:bool;

data my_int = #NUM of int
            | #NIL;

var m:my_int;

fun foo() : int
{
   fun foo2() : int
   {
      z := true;
      return 0;
   };

   var x,y[2]:int;
   x := 1;
   y[0] := 1;
   y[1] := 2;
   x := foo();
   return foo2();
};

fun bar(x:int, y[]:int):bool
{
   {
      var x, y:real;
      x:=1.0;
      y:=2.0;
      print x+y;
   };

   print x+y[1];
   return true;
};


fun check(x:my_int):bool
{
   var b:bool;
   case x of {
      #NUM (n) => b := n > 2
      | #NIL => b:= false
   };
   return b;
};

begin 
x[1]:= x[2]+x[1]+foo();
z := true && false;
z := not z;
z := true || false;
z := y < y1;
z := y > y1;
z := y =< y1;
z := y >= y1;
y := y1 + y1;
y := y1 - y1;
y := y1 * y1;
y := y1 / y1;
y := -y1;
y := floor(y1);
y := ceil(y1);
x1 := size(x);
y := float(x1);
m:=#NIL;
z:=check(m);
z:=bar(x[0], x);
end
