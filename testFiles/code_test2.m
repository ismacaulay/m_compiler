/*
Checks that arrays and passing arrays by reference works correctly
*/
var a:int;
var x[2][2]:int;

fun foo(x[][]:int, z:int):int
{
   x[0][0] := 42;
   x[1][2] := 14;
   return z;
};

a := 21;
a := foo(x, a);
print x[0][0];
