/*
 Tests that basic code works except for arrays and constructors
*/
var x:int;
var x1:int;
var x2:int;
var y:real;
var y2:real;
var z:bool;
var a:char;

fun func():int
{
   fun func2():int
   {
      return 42;
   };

   fun func3():int
   {
      fun func4():int
      {
         return func2();
      };

      return func4();
   };

   return func3();
};

fun foo() : int
{
   fun bar() : int
   {
      var x, y, z:int;

      fun hello():char
      {
         var b:char;
         b:="3";
         return b;
      };
      x := 1;
      y := 2;
      a := "2";
      print a;
      print "\n";
      if x=y then z:= 12
         else z:=13;

      a := hello();

      while x =< 10 do
      {
         print "h";
         print "e";
         print "l";
         print "l";
         print "o";
         print "\n";
         x := x + 1;
      };
      return z;
   };

   return bar();
};

fun max(x:int, y:int):int
{
   var z:int;
   if x > y then z := x
      else z := y;
   
   return z;
};

fun rec(x:int):int
{
   var r:int;
   if x=0 then r:=x
      else {
         print x;
         r := rec(x-1);
      };

   return r;
};

a := "0";
print a;
print "\n";

x := foo();
print x;
print a;

if x > 12 then z:=true
   else z:= false;
print z;

while x >= 0 do
{
   print x;
   x := x - 1;
};

x := max(3, 4);
print x;

x := 2*3;
print x;
x := 10/2;
print x;
x := 2+3;
print x;
x := 2-3;
print x;
x := -x;
print x;

x1 := 2;
x2 := 3;
z := x1 < x2;
print z;
z := x1 > x2;
print z;
z := x1 =< x2;
print z;
z := x1 >= x2;
print z;

z := true && false;
print z;
z := true || false;
print z;
z := not true;
print z;

y := 1.25;
x := floor(y);
print x;
x := ceil(y);
print x;
y2 := float(x1);
print y2;

x := rec(5);
print x;

print func();

