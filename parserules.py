#
# module: parserules.py
#
#  This module contains all of the grammer rules used in the M language
#  which is used by yacc.py. It constructs an Abstract Syntax Tree
#  which is defined in ast.py. Each function contains one production rule 
#  (unless all the rules returned the value). Parse errors will raise
#  the exception ParseError which is defined in errors.py
#
from lexrules import tokens
from ast import *
from errors import ParseError

def p_prog(p):
   'prog : block'
   (decls, stmts) = p[1]
   p[0] = Prog(decls, stmts)

def p_block(p):
   'block : declarations program_body'
   p[0] = (p[1], p[2])

########################################
# Declarations
########################################
def p_declarations(p):
   'declarations : declaration SEMICOLON declarations'
   decls = []
   decls.extend(p[1])
   decls.extend(p[3])
   p[0] = decls

def p_declarations_empty(p):
   'declarations : empty'
   p[0] = []

def p_declaration_var(p):
   '''declaration : var_declaration
                  | fun_declaration
                  | data_declaration'''
   p[0] = p[1]

def p_var_declaration(p):
   'var_declaration : VAR var_specs COLON type'
   var_specs = p[2]
   m_type = p[4]
   var_decls = []
   for (name, exprs) in var_specs:
      var_decls.append(Var(name, exprs, m_type))

   p[0] = var_decls

def p_var_specs(p):
   'var_specs : var_spec more_var_specs'
   var_specs = [p[1]]
   var_specs.extend(p[2])
   p[0] = var_specs

def p_more_var_specs(p):
   'more_var_specs : COMMA var_spec more_var_specs'
   var_specs = [p[2]]
   var_specs.extend(p[3])
   p[0] = var_specs

def p_more_var_specs_empty(p):
   'more_var_specs : empty'
   p[0] = []

def p_var_spec(p):
   'var_spec : ID array_dimensions'
   p[0] = (p[1], p[2])

def p_array_dimensions(p):
   'array_dimensions : SLPAR expr SRPAR array_dimensions'
   exprs = [p[2]]
   exprs.extend(p[4])
   p[0] = exprs

def p_array_dimensions_empty(p):
   'array_dimensions : empty'
   p[0] = []

def p_type_int(p):
   'type : INT'
   p[0] = Type("int")

def p_type_real(p):
   'type : REAL'
   p[0] = Type("real")

def p_type_bool(p):
   'type : BOOL'
   p[0] = Type("bool")

def p_type_char(p):
   'type : CHAR'
   p[0] = Type("char")

def p_type_id(p):
   'type : ID'
   p[0] = Type(p[1])

#########################################
# Functions
#########################################
def p_fun_declaration(p):
   'fun_declaration : FUN ID param_list COLON type CLPAR fun_block CRPAR'
   (block, stmts) = p[7]
   p[0] = [Fun(p[2], p[3], p[5], block, stmts)]

def p_fun_block(p):
   'fun_block : declarations fun_body'
   p[0] = (p[1], p[2])

def p_param_list(p):
   'param_list : LPAR parameters RPAR'
   p[0] = p[2]

def p_parameters(p):
   'parameters : basic_declaration more_parameters'
   params = [p[1]]
   params.extend(p[2])
   p[0] = params

def p_parameters_empty(p):
   'parameters : empty'
   p[0] = []

def p_more_parameters(p):
   'more_parameters : COMMA basic_declaration more_parameters'
   params = [p[2]]
   params.extend(p[3])
   p[0] = params

def p_more_parameters_empty(p):
   'more_parameters : empty'
   p[0] = []

def p_basic_declaration(p):
   'basic_declaration : ID basic_array_dimensions COLON type'
   p[0] = Parameter(p[1], p[2], p[4])

def p_basic_array_dimensions(p):
   'basic_array_dimensions : SLPAR SRPAR basic_array_dimensions'
   dim = 1 + p[3]
   p[0] = dim

def p_basic_array_dimensions_empty(p):
   'basic_array_dimensions : empty'
   p[0] = 0

#########################################
# Data Types
#########################################
def p_data_declaration(p):
   'data_declaration : DATA ID EQUAL cons_declarations'
   p[0] = [Data(p[2], p[4])]

def p_cons_declarations(p):
   'cons_declarations : cons_decl more_cons_decl'
   cons_decls = [p[1]]
   cons_decls.extend(p[2])
   p[0] = cons_decls

def p_more_cons_decl(p):
   'more_cons_decl : SLASH cons_decl more_cons_decl'
   cons_decls = [p[2]]
   cons_decls.extend(p[3])
   p[0] = cons_decls

def p_more_cons_decl_empty(p):
   'more_cons_decl : empty'
   p[0] = []

def p_cons_decl_cid_of(p):
   'cons_decl : CID OF type_list'
   p[0] = Constructor(p[1], p[3])

def p_cons_decl_cid(p):
   'cons_decl : CID'
   p[0] = Constructor(p[1], [])

def p_type_list(p):
   'type_list : type more_type'
   types = [p[1]]
   types.extend(p[2])
   p[0] = types

def p_more_type(p):
   'more_type : MUL type more_type'
   types = [p[2]]
   types.extend(p[3])
   p[0] = types

def p_more_type_empty(p):
   'more_type : empty'
   p[0] = []

#########################################
# Statements
#########################################
def p_program_body(p):
   'program_body : BEGIN prog_stmts END'
   p[0] = p[2]

def p_program_body2(p):
   'program_body : prog_stmts'
   p[0] = p[1]

def p_fun_body(p):
   'fun_body : BEGIN prog_stmts RETURN expr SEMICOLON END'
   stmts = p[2]
   stmts.append(Return(p[4], str(p.lineno(4))))
   p[0] = stmts

def p_fun_body2(p):
   'fun_body : prog_stmts RETURN expr SEMICOLON'
   stmts = p[1]
   stmts.append(Return(p[3], str(p.lineno(3))))
   p[0] = stmts

def p_prog_stmts(p):
   'prog_stmts : prog_stmt SEMICOLON prog_stmts'
   stmts = [p[1]]
   stmts.extend(p[3])
   p[0] = stmts

def p_prog_stmts_empty(p):
   'prog_stmts : empty'
   p[0] = []

def p_prog_stmt(p):
   'prog_stmt : IF expr THEN prog_stmt ELSE prog_stmt'
   p[0] = Cond(p[2], p[4], p[6], str(p.lineno(2)))

def p_prog_stmt_while(p):
   'prog_stmt : WHILE expr DO prog_stmt'
   p[0] = While(p[2], p[4], str(p.lineno(2)))

def p_prog_stmt_read(p):
   'prog_stmt : READ location'
   (var, exprs) = p[2]
   p[0] = Read(var, exprs, str(p.lineno(2)))

def p_prog_stmt_assign(p):
   'prog_stmt : location ASSIGN expr'
   (var, exprs) = p[1]
   p[0] = Assign(var, exprs, p[3], str(p.lineno(2)))

def p_prog_stmt_print(p):
   'prog_stmt : PRINT expr'
   p[0] = Print(p[2], str(p.lineno(2)))

def p_prog_stmt_block(p):
   'prog_stmt : CLPAR block CRPAR'
   (decls, stmts) = p[2]
   p[0] = Block(decls, stmts)

def p_prog_stmt_case(p):
   'prog_stmt : CASE expr OF CLPAR case_list CRPAR'
   p[0] = Case(p[2], p[5], str(p.lineno(2)))

def p_location(p):
   'location : ID array_dimensions'
   p[0] = (p[1], p[2])

def p_case_list(p):
   'case_list : case more_case'
   cases = [p[1]]
   cases.extend(p[2])
   p[0] = cases

def p_more_case(p):
   'more_case : SLASH case more_case'
   cases = [p[2]]
   cases.extend(p[3])
   p[0] = cases

def p_more_case_empty(p):
   'more_case : empty'
   p[0] = []

def p_case(p):
   'case : CID var_list ARROW prog_stmt'
   p[0] = CaseStmt(p[1], p[2], p[4], str(p.lineno(4)))

def p_var_list(p):
   'var_list : LPAR var_list1 RPAR'
   p[0] = p[2]

def p_var_list_empty(p):
   'var_list : empty'
   p[0] = []

def p_var_list1(p):
   'var_list1 : ID more_var_list1'
   var_list = [p[1]]
   var_list.extend(p[2])
   p[0] = var_list

def p_more_var_list1(p):
   'more_var_list1 : COMMA ID more_var_list1'
   var_list = [p[2]]
   var_list.extend(p[3])
   p[0] = var_list

def p_more_var_list1_empty(p):
   'more_var_list1 : empty'
   p[0] = []

#########################################
# Expressions
#########################################
def p_expr_or(p):
   'expr : expr OR bint_term'
   exprs = [p[1]]
   exprs.append(p[3])
   p[0] = App(Op("Or"), exprs, str(p.lineno(2))) 

def p_expr_bint_term(p):
   'expr : bint_term'
   p[0] = p[1]

def p_bint_term_and(p):
   'bint_term : bint_term AND bint_factor'
   exprs = [p[1]]
   exprs.append(p[3])
   p[0] = App(Op("And"), exprs, str(p.lineno(2)))

def p_bint_term_bint_factor(p):
   'bint_term : bint_factor'
   p[0] = p[1]

def p_bint_factor_not(p):
   'bint_factor : NOT bint_factor'
   exprs = [p[2]]
   p[0] = App(Op("Not"), exprs, str(p.lineno(1)))

def p_bint_factor_compare_op(p):
   'bint_factor : int_expr compare_op int_expr'
   exprs = [p[1]]
   exprs.append(p[3])
   p[0] = App(p[2], exprs, str(p.lineno(2)))

def p_bint_factor_int_expr(p):
   'bint_factor : int_expr'
   p[0] = p[1]

def p_compare_op_equal(p):
   'compare_op : EQUAL'
   p[0] = Op("Eq")

def p_compare_op_lt(p):
   'compare_op : LT'
   p[0] = Op("Lt")

def p_compare_op_gt(p):
   'compare_op : GT'
   p[0] = Op("Gt")

def p_compare_op_le(p):
   'compare_op : LE'
   p[0] = Op("Le")

def p_compare_op_ge(p):
   'compare_op : GE'
   p[0] = Op("Ge")

def p_int_expr_addop(p):
   'int_expr : int_expr addop int_term'
   exprs = [p[1]]
   exprs.append(p[3])
   p[0] = App(p[2], exprs, str(p.lineno(2)))

def p_int_expr_int_term(p):
   'int_expr : int_term'
   p[0] = p[1]

def p_addop_add(p):
   'addop : ADD'
   p[0] = Op("Add")

def p_addop_sub(p):
   'addop : SUB'
   p[0] = Op("Sub")

def p_int_term_mulop(p):
   'int_term : int_term mulop int_factor'
   exprs = [p[1]]
   exprs.append(p[3])
   p[0] = App(p[2], exprs, str(p.lineno(2)))

def p_int_term_int_factor(p):
   'int_term : int_factor'
   p[0] = p[1]

def p_mulop_mul(p):
   'mulop : MUL'
   p[0] = Op("Mul")

def p_mulop_div(p):
   'mulop : DIV'
   p[0] = Op("Div")

def p_int_factor_expr(p):
   'int_factor : LPAR expr RPAR'
   p[0] = p[2]

def p_int_factor_size(p):
   'int_factor : SIZE LPAR ID basic_array_dimensions RPAR'
   p[0] = Size(p[3], p[4], str(p.lineno(1)))

def p_int_factor_float(p):
   'int_factor : FLOAT LPAR expr RPAR'
   exprs = [p[3]]
   p[0] = App(Op("Float"), exprs, str(p.lineno(1)))

def p_int_factor_floor(p):
   'int_factor : FLOOR LPAR expr RPAR'
   exprs = [p[3]]
   p[0] = App(Op("Floor"), exprs, str(p.lineno(1)))

def p_int_factor_ceil(p):
   'int_factor : CEIL LPAR expr RPAR'
   exprs = [p[3]]
   p[0] = App(Op("Ceil"), exprs, str(p.lineno(1)))

def p_int_factor_id(p):
   'int_factor : ID modifier_list'
   node = p[2]
   if isinstance(node, App):
      node.op.name = p[1]
   else:
      node.var = p[1]
   p[0] = node

def p_int_factor_cid(p):
   'int_factor : CID cons_argument_list'
   p[0] = App(CId(p[1]), p[2], str(p.lineno(1)))

def p_int_factor_ival(p):
   'int_factor : IVAL'
   p[0] = IVal(p[1], str(p.lineno(1)))

def p_int_factor_rval(p):
   'int_factor : RVAL'
   p[0] = RVal(p[1], str(p.lineno(1)))

def p_int_factor_bval(p):
   'int_factor : BVAL'
   p[0] = BVal(p[1], str(p.lineno(1)))

def p_int_factor_cval(p):
   'int_factor : CVAL'
   p[0] = CVal(p[1], str(p.lineno(1)))

def p_int_factor_sub(p):
   'int_factor : SUB int_factor'
   exprs = [p[2]]
   p[0] = App(Op("Neg"), exprs, str(p.lineno(1)))

def p_modifier_list(p):
   'modifier_list : fun_argument_list'
   p[0] = App(Fn(""), p[1], str(p.lineno(1)))

def p_modifier_list_arr_dim(p):
   'modifier_list : array_dimensions'
   p[0] = Id("", p[1], str(p.lineno(1)))

def p_fun_argument_list(p):
   'fun_argument_list : LPAR arguments RPAR'
   p[0] = p[2]

def p_cons_argument_list(p):
   'cons_argument_list : fun_argument_list'
   p[0] = p[1]

def p_cons_argument_list_empty(p):
   'cons_argument_list : empty'
   p[0] = []

def p_arguments(p):
   'arguments : expr more_arguments'
   exprs = [p[1]]
   exprs.extend(p[2])
   p[0] = exprs

def p_arguments_empty(p):
   'arguments : empty'
   p[0] = []

def p_more_arguments(p):
   'more_arguments : COMMA expr more_arguments'
   exprs = [p[2]]
   exprs.extend(p[3])
   p[0] = exprs

def p_more_arguments_empty(p):
   'more_arguments : empty'
   p[0] = []

#########################################
# Empty Rule
#########################################
def p_empty(p):
   'empty :'
   pass

#########################################
# Error Handler
#########################################
def p_error(p):
   if p:
      raise ParseError("Syntax Error [" + str(p.lineno) + "]: Unexpected token '" + str(p.value) + "' encountered")
   else:
      raise ParseError("Error: Missing tokens at end of file")

