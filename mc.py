#!/usr/bin/env python2
import sys

import ply.lex as lex
import ply.yacc as yacc

import lexrules
import parserules
from errors import CompilationError, MultilineCommentError
from semantics import SemanticChecker
from irgenerator import IRGenerator
from codegenerator import CodeGenerator

def main():
   # Check command line args
   if len(sys.argv) != 2:
      print "Usage: ./mc.py [filepath]"
      sys.exit(1)

   # Get the data from the file
   try:
      filepath = sys.argv[1]
      data = open(filepath).read()
   except Exception:
      print "Error: Unable to open file " + filepath
      sys.exit(1)

   try:
      print "Compiling M++ program..."
      print "  Parsing " + str(filepath) + "...",
      # Create the lexer and parser
      lex.lex(module=lexrules)
      parser = yacc.yacc(module=parserules)
      # Parse the data and get an ast
      ast = parser.parse(data, tracking=True)
      # Check if all the multiline comments have been closed
      if lexrules.inMultilineCommentState:
         raise MultilineCommentError()
      print "done"

      print "  Checking Semantics...",
      semantic_checker = SemanticChecker()
      semantic_checker.check_semantics(ast)
      print "done"
      
      print "  Generating intermediate representation...",
      ir_generator = IRGenerator()
      ir = ir_generator.generate(ast)
      print "done"

      print "  Generating stack code...",
      code_generator = CodeGenerator()
      code = code_generator.generate(ir)
      print "done"

      output_filepath = filepath.rsplit('.', 1)[0] + '.am'
      print "  Saving stack code to " + output_filepath + '...',
      output = open(output_filepath, 'w')
      output.write(code)
      output.close()
      print "done"
      print "Complete"
   except CompilationError as e:
      print "error\n"
      print e

if __name__ == "__main__":
   main()

