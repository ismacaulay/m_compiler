#
# module: semantics.py
#
from ast import *
from symboltable import *
from errors import *

class SemanticChecker():
   def __init__(self):
      self._symboltable = SymbolTable()

   def check_semantics(self, ast):
      # check that the top level of the ast is a Prog
      if not isinstance(ast, Prog):
         raise AstTypeError()
      # process the ast
      self._process_program_scope(ast)
      # if there is anything left in the symbol table, something went wrong
      if len(self._symboltable) > 0:
         raise SymbolTableNotEmptyError()

#################################################
# Scope functions
#################################################
   def _process_program_scope(self, prog):
      '''
      Processes the program scope level
      '''
      self._symboltable.new_scope(ProgScope())
      self._process_scope(prog.decls, prog.stmts)
      self._symboltable.remove_scope()

   def _process_function_scope(self, function):
      '''
      Processes a function scope level
      '''
      self._symboltable.new_scope(FunScope(function.m_type))
      # add function arguments first, they need to be added in reverse order
      for param in reversed(function.params):
         self._add_fun_arg_to_st(param)
      # process the rest of the function
      self._process_scope(function.decls, function.stmts)
      self._symboltable.remove_scope()

   def _process_block_scope(self, block):
      '''
      Processes a block scope level
      '''
      self._symboltable.new_scope(BlockScope())
      self._process_scope(block.decls, block.stmts)
      self._symboltable.remove_scope()

   def _process_case_scope(self, case):
      '''
      Processes a case scope level
      '''
      case_type = self._process_expr(case.expr)
      for c in case.case_stmts:
         # lookup the constructor
         constructor = self._symboltable.lookup(c.cid)
         if not isinstance(constructor, IConstructor):
            raise LookupSymbolTypeError(str(constructor), "IConstructor")

         # ensure that the constructor type and the case type match
         if not self._types_match(case_type, constructor.user_type):
            raise InvalidTypeError("Trying to use a constructor of type " + str(constructor.user_type) + " in a case statement that is using type " + str(case_type), c.lineno)

         # ensure the number of vars given matches that of the constructor
         if not len(constructor.arg_types) == len(c.var_list):
            raise InvalidTypeError("Trying to use a constructor that expects " + str(len(constructor.arg_types)) + " arguments with " + str(len(c.var_list)) + " arguments", c.lineno)
         # create a new scope for the case
         self._symboltable.new_scope(CaseScope())
         # add the case variables to the scope
         for (var, t) in zip(c.var_list, constructor.arg_types):
            self._symboltable.insert(Variable(var, t, 0))
         # process the case statement
         self._process_stmt(c.stmt)
         # remove the case scope since we are moving onto the next case
         self._symboltable.remove_scope()
      
   def _process_scope(self, scope_decls, scope_stmts):
      '''
      Process a the declarations and statements of a scope level
      '''
      for decl in scope_decls:
         self._process_decl(decl)     

      for stmt in scope_stmts:
         self._process_stmt(stmt)

      for decl in scope_decls:
         if isinstance(decl, Fun):
            self._process_function_scope(decl)

#################################################
# Declaration functions
#################################################
   def _process_decl(self, decl):
      '''
      Processes a declaration and adds it to the symbol table
      '''
      if isinstance(decl, Var):
         self._add_var_decl_to_st(decl)
      elif isinstance(decl, Fun):
         self._add_fun_decl_to_st(decl)
      elif isinstance(decl, Data):
         self._add_data_decl_to_st(decl)

   def _add_var_decl_to_st(self, var_decl):
      '''
      Adds a variable to the symbol table
      '''
      # process the array expressions first
      num_dims = self._process_array_dims_exprs(var_decl.array_dims)
      self._symboltable.insert(Variable(var_decl.name, var_decl.m_type, num_dims))

   def _add_fun_decl_to_st(self, fun_decl):
      '''
      Adds a function to the symbol table
      '''
      # process the function arguments first
      arg_types = self._process_function_arguments(fun_decl.params)
      self._symboltable.insert(Function(fun_decl.name, arg_types, fun_decl.m_type))

   def _add_fun_arg_to_st(self, fun_arg):
      '''
      Adds a function argument to the symbol table
      '''
      self._symboltable.insert(Argument(fun_arg.name, fun_arg.m_type, fun_arg.arr_dim))

   def _add_data_decl_to_st(self, data_decl):
      '''
      Adds a user defined type to the symbol table, then adds all of its constructors
      '''
      # add user type
      self._symboltable.insert(DataType(data_decl.name))
      # add constructors
      for con in data_decl.cons:
         self._symboltable.insert(Constructor(con.name, con.types, data_decl.name))

   def _process_function_arguments(self, parameters):
      '''
      Processes the function arguments and returns a list of argument types
      '''
      arg_types = []
      for param in parameters:
         arg_types.append((param.m_type, param.arr_dim))
      return arg_types

#################################################
# Statement functions
#################################################
   def _process_stmt(self, stmt):
      '''
      Processes a statement based on they type of statement
      '''
      if isinstance(stmt, Assign):
         self._process_assign_stmt(stmt)
      elif isinstance(stmt, While):
         self._process_while_stmt(stmt)
      elif isinstance(stmt, Cond):
         self._process_cond_stmt(stmt)
      elif isinstance(stmt, Read):
         self._process_read_stmt(stmt)
      elif isinstance(stmt, Print):
         self._process_print_stmt(stmt)
      elif isinstance(stmt, Return):
         self._process_return_stmt(stmt)
      elif isinstance(stmt, Block):
         self._process_block_scope(stmt)
      elif isinstance(stmt, Case):
         self._process_case_scope(stmt)

   def _process_assign_stmt(self, stmt):
      '''
      Processes an assignment statement. Both the LHS and RHS of the statment must have matching types
      '''
      # lookup the variable
      var_desc = self._symboltable.lookup(stmt.name)
      if not isinstance(var_desc, IVariable):
         raise LookupSymbolTypeError(str(var_desc), "IVariable")

      # check that the array dimensions are valid
      if self._check_array_dims(stmt.array_dims, var_desc.array_dims):
         # process the RHS of the statment
         expr_type = self._process_expr(stmt.expr)
         if not self._types_match(expr_type, var_desc.m_type):
            raise InvalidTypeError("Trying to assign an " + str(expr_type) + " to a " + str(var_desc.m_type), stmt.lineno)
      else:
         raise ArrayAccessError(len(stmt.array_dims), var_desc.array_dims, stmt.lineno)

   def _process_while_stmt(self, stmt):
      '''
      Processes a while statement. The conditional expression must be a bool type
      '''
      # process conditional
      expr_type = self._process_expr(stmt.expr)
      if self._types_match(expr_type, "bool"):
         # process do statment
         self._process_stmt(stmt.stmt)
      else:
         raise InvalidTypeError("Trying to use a " + str(expr_type) + " in a while statement conditional", stmt.lineno)

   def _process_cond_stmt(self, stmt):
      '''
      Processes and if statement. The if conditional must be a bool type
      '''
      # process conditional
      expr_type = self._process_expr(stmt.expr)
      if self._types_match(expr_type, "bool"):
         # process then and else statments
         self._process_stmt(stmt.then_stmt)
         self._process_stmt(stmt.else_stmt)
      else:
         raise InvalidTypeError("Trying to use a " + str(expr_type) + " in an if statement conditional", stmt.lineno)

   def _process_read_stmt(self, stmt):
      '''
      Processes a read statment
      '''
      # lookup the variable to read to
      var_desc = self._symboltable.lookup(stmt.var)
      if not isinstance(var_desc, IVariable):
         raise LookupSymbolTypeError(str(var_desc), "IVariable")

      if not self._is_simple_type(var_desc.m_type):
         raise InvalidTypeError("Trying to use read with type " + str(var_desc.m_type), stmt.lineno)

      # check that the array dimensions are being used correctly
      if not self._check_array_dims(stmt.array_dims, var_desc.array_dims):
         raise ArrayAccessError(len(assign.array_dims), var_desc.array_dims, stmt.lineno)

   def _process_print_stmt(self, stmt):
      '''
      Processes a print statment
      '''
      expr_type = self._process_expr(stmt.expr)

      if not self._is_simple_type(expr_type):
         raise InvalidTypeError("Trying to use print with type " + str(expr_type), stmt.lineno)


   def _process_return_stmt(self, stmt):
      '''
      Processes a return statment. The return type and function return type must match
      '''
      # get the current scope return type
      return_type = self._symboltable.return_type()
      # process return expr
      expr_type = self._process_expr(stmt.expr)
      if not self._types_match(return_type, expr_type):
         raise InvalidTypeError("Returning " + str(expr_type) + " from function with return type " + str(return_type), stmt.lineno)

#################################################
# Expression functions
#################################################
   def _process_expr(self, expr, func_arg_array_dims=0):
      '''
      Processes and expression. If an expression is a IVal, RVal, BVal, or CVal the type is just returned,
      otherwise more processing has to be done
      '''
      if isinstance(expr, IVal):
         return "int"
      elif isinstance(expr, RVal):
         return "real"
      elif isinstance(expr, BVal):
         return "bool"
      elif isinstance(expr, CVal):
         return "char"
      elif isinstance(expr, Size):
         return self._process_size_expr(expr)
      elif isinstance(expr, Id):
         return self._process_id_expr(expr, func_arg_array_dims)
      elif isinstance(expr, App):
         return self._process_app_expr(expr)

   def _process_size_expr(self, expr):
      '''
      Process the built in function 'size'. The array dimensions used in size must be less than that of the variable
      Returns the type 'int' if successful
      '''
      desc = self._symboltable.lookup(expr.var)
      if not isinstance(desc, IVariable):
         raise LookupSymbolTypeError(str(desc), "IVariable")

      if expr.num_dims <= desc.array_dims-1:
         return "int"

      raise InvalidTypeError("Trying to check the size of dimension " + str(expr.num_dims + 1) + " using a " + str(desc.array_dims) + " dimensonal array", expr.lineno)

   def _process_id_expr(self, expr, func_arg_array_dims=0):
      '''
      Processes an id expression.
      Returns the id type if successful
      '''
      desc = self._symboltable.lookup(expr.var)
      if not isinstance(desc, IVariable):
         raise LookupSymbolTypeError(str(desc), "IVariable")

      if func_arg_array_dims > 0:
         if func_arg_array_dims == desc.array_dims:
            return desc.m_type
         else:
            raise InvalidTypeError("Trying to pass an array with " + str(desc.array_dims) + " dimensions to a function expecting an array with " + str(func_arg_array_dims) + " dimensions", expr.lineno)
            
      if self._check_array_dims(expr.array_dims, desc.array_dims):
         return desc.m_type

      raise ArrayAccessError(len(expr.array_dims), desc.array_dims, expr.lineno)

   def _process_app_expr(self, expr):
      '''
      Processes and apply expression.
      Returns the type of the resulting operation if successful
      '''
      op = expr.op
      if isinstance(op, Fn):
         return self._process_fn_expr(expr)
      elif isinstance(op, CId):
         return self._process_cid_expr(expr)
      elif isinstance(op, Op):
         if self._is_arithmetic_op(op.op):
            return self._process_arithmetic_expr(expr)
         elif self._is_comparison_op(op.op):
            return self._process_comparison_expr(expr)
         elif self._is_boolean_op(op.op):
            return self._process_boolean_expr(expr)
         elif self._is_builtin_func(op.op):
            return self._process_builtin_expr(expr)

   def _process_fn_expr(self, expr):
      '''
      Processes a function call expression.
      Returns the function return type if successful
      '''
      # lookup function
      desc = self._symboltable.lookup(expr.op.name)
      if not isinstance(desc, IFunction):
         raise LookupSymbolTypeError(str(desc), "IFunction")

      if len(desc.arg_types) != len(expr.exprs):
         raise InvalidTypeError("Function " + str(expr.op.name) + " takes " + str(len(desc.arg_types)) + " arguments but was given " + str(len(expr.exprs)), expr.lineno)
      # ensure the argument types used match that of the function
      for ((arg_type, array_dims), e) in zip(desc.arg_types, expr.exprs):
         expr_type = self._process_expr(e, array_dims)
         if not self._types_match(expr_type, arg_type):
            raise InvalidTypeError("Function expects type " + str(arg_type) + " but was given " + str(expr_type), expr.lineno)
      
      return desc.return_type

   def _process_cid_expr(self, expr):
      '''
      Processes a constructor expression
      Returns the user defined type if successful
      '''
      # lookup the constructor
      desc = self._symboltable.lookup(expr.op.name)
      if not isinstance(desc, IConstructor):
         raise LookupSymbolTypeError(str(desc), "IConstructor")

      # ensure that the argument types used matches that of the constructor
      for (arg_type, e) in zip(desc.arg_types, expr.exprs):
         expr_type = self._process_expr(e)
         if not self._types_match(expr_type, arg_type):
            raise InvalidTypeError("Constructor expects type " + str(arg_type) + " but was given " + str(expr_type), expr.lineno)
      
      return desc.user_type

   def _process_arithmetic_expr(self, expr):
      '''
      Process an arithmetic expression. All the types used in the expression must match. 
      Only real and int types can be used.
      Returns the resulting type
      '''
      arithmetic_type = None
      for e in expr.exprs:
         expr_type = self._process_expr(e)
         if arithmetic_type == None:
            if self._types_match(expr_type, "int") or self._types_match(expr_type, "real"):
               arithmetic_type = expr_type
            else:
               raise InvalidTypeError("Trying to perform an arithmetic operation on a " + str(expr_type), expr.lineno)
         elif not self._types_match(arithmetic_type, expr_type):
            raise InvalidTypeError("Trying to perform an arithmetic operation on a " + str(expr_type) + " and a " + str(arithmetic_type), expr.lineno)
      return arithmetic_type

   def _process_comparison_expr(self, expr):
      '''
      Processes a comparison expression. All the types used in the expression must match.
      Bools can not be used in a comparison expression.
      Returns a bool type
      '''
      comparison_type = None
      for e in expr.exprs:
         expr_type = self._process_expr(e)
         if comparison_type == None:
            if self._types_match(expr_type, "bool"):
               raise InvalidTypeError("Trying to perform a comparison operation on a bool", expr.lineno)
            else:
               comparison_type = expr_type
         elif not self._types_match(comparison_type, expr_type):
            raise InvalidTypeError("Trying to perform a comparison operation on a " + str(comparison_type) + " and a " + str(expr_type), expr.lineno)
      return "bool"

   def _process_boolean_expr(self, expr):
      '''
      Processes a boolean expression. All the types in the expression must be a bool
      Returns a bool type
      '''
      for e in expr.exprs:
         expr_type = self._process_expr(e)
         if not self._types_match(expr_type, "bool"):
            raise InvalidTypeError("Trying to perform a boolean operation on a " + str(expr_type), expr.lineno)
      return "bool"

   def _process_builtin_expr(self, expr):
      '''
      Processes the built in functions floor, ceil, and float.
      The floor and ceil functions must have a real for the argument.
      The float function must have an int for the argument.
      Returns a real type for all builtins functions
      '''
      # process a floor or ceil function
      if self._types_match(expr.op, "Floor") or self._types_match(expr.op, "Ceil"):
         for e in expr.exprs:
            expr_type = self._process_expr(e)
            if not self._types_match(expr_type, "real"):
               raise InvalidTypeError("Trying to perform a " + str(expr.op).lower() + " on a " + str(expr_type), expr.lineno)
         return "int"
      # process a float function
      elif self._types_match(expr.op, "Float"):
         for e in expr.exprs:
            expr_type = self._process_expr(e)
            if not self._types_match(expr_type, "int"):
               raise InvalidTypeError("Trying to perform a float on a " + str(expr_type), expr.lineno)
         return "real"

   def _process_array_dims_exprs(self, array_dims_exprs):
      '''
      Processes the expressions used in an array access. All the types must be an int
      Returns the number of dimensions in the array
      '''
      for expr in array_dims_exprs:
         expr_type = self._process_expr(expr)
         if not self._types_match(expr_type, "int"):
            raise InvalidTypeError("Trying to access an array with a " + str(expr_type), expr.lineno)
      return len(array_dims_exprs)

#################################################
# Helper functions
#################################################
   def _check_array_dims(self, array_dims_exprs, expected_num_dims):
      '''
      Returns true if the number of dimensions being accessed is less than or equal to 
      the number of array dimensions available.
      '''
      num_dims = self._process_array_dims_exprs(array_dims_exprs)
      return num_dims == expected_num_dims

   def _types_match(self, actual, expected):
      '''
      Returns true if the types match
      '''
      return str(actual) == str(expected)

   def _is_arithmetic_op(self, op):
      '''
      Returns true if the operation is an arithmetic operation
      '''
      return self._types_match(op, "Add") or self._types_match(op, "Sub") or self._types_match(op, "Mul") or self._types_match(op, "Div") or self._types_match(op, "Neg")

   def _is_comparison_op(self, op):
      '''
      Returns true if the operation is a comparison operation
      '''
      return self._types_match(op, "Lt") or self._types_match(op, "Le") or self._types_match(op, "Gt") or self._types_match(op, "Ge") or self._types_match(op, "Eq")

   def _is_boolean_op(self, op):
      ''' 
      Returns true if the operation is a boolean operation
      '''
      return self._types_match(op, "Not") or self._types_match(op, "And") or self._types_match(op, "Or")

   def _is_builtin_func(self, fun):
      '''
      Returns true if the function is a built in function name
      '''
      return self._types_match(fun, "Float") or self._types_match(fun, "Floor") or self._types_match(fun, "Ceil")

   def _is_simple_type(self, t):
      return self._types_match(t, "int") or self._types_match(t, "real") or self._types_match(t, "bool") or self._types_match(t, "char")
