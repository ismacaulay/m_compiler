#
# module: errors.py
#
#  This module contains the exceptions used by parserules.py
#  and lexrules.py when an error in parsing/lexing occur
#
#########################################
# CompilationError
#     Base class for custom errors
#########################################
class CompilationError(Exception):
   def __init__(self):
      pass

#########################################
# Lexing Errors
#########################################
class LexError(CompilationError):
   '''
   An exception to represent an error when lexing

   msg: the error message provided by the lexer
   '''
   def __init__(self, msg):
      self.msg = msg
   def __str__(self):
      return str(self.msg)

#########################################
# Parsing Errors
#########################################
class ParseError(CompilationError):
   '''
   An exception to represent an error when parsing

   msg: the error message provided by the parser
   '''
   def __init__(self, msg):
      self.msg = msg
   def __str__(self):
      return str(self.msg)

class MultilineCommentError(CompilationError):
   def __init__(self):
      pass
   def __str__(self):
      return "Parse Error: Multiline Comments not closed correctly"

#########################################
# Symbol Table Errors
#########################################
class NoScopeDefinedError(CompilationError):
   def __init__(self):
      pass
   def __str__(self):
      return "Symbol Table Error: Trying to insert when there is no scope level"

class SymbolNotFoundError(CompilationError):
   def __init__(self, symbol):
      self.symbol = symbol
   def __str__(self):
      return "Symbol Table Error: The symbol '" + str(self.symbol) + "' was not found in the symbol table"

class SymbolRedefinedError(CompilationError):
   def __init__(self, symbol):
      self.symbol = symbol
   def __str__(self):
      return "Symbol Table Error: The symbol '" + str(self.symbol) + "' has already been defined"

class UserDefinedTypeNotFoundError(CompilationError):
   def __init__(self, user_type):
      self.user_type = user_type
   def __str__(self):
      return "Symbol Table Error: The user type '" + str(self.user_type) + "' was not found in the symbol table"

class ReturnFromNonFunctionError(CompilationError):
   def __init__(self):
      pass
   def __str__(self):
      return "Symbol Table Error: Trying to get return type for non function"

#########################################
# Semantic Errors
#########################################
class InvalidTypeError(CompilationError):
   def __init__(self, msg, lineno):
      self.msg = msg
      self.lineno = lineno
   def __str__(self):
      return "Type Error [" + str(self.lineno) + "]: " + str(self.msg)

class ArrayAccessError(CompilationError):
   def __init__(self, access_dims, array_dims, lineno):
      self.array_dims = array_dims
      self.access_dims = access_dims
      self.lineno = lineno
   def __str__(self):
      if self.access_dims > self.array_dims: 
         return "Array Access Error [" + str(self.lineno) + "]: Trying to access dimension " + str(self.access_dims) + " of a " + str(self.array_dims) + " dimensional array"
      else:
         return "Array Access Error [" + str(self.lineno) + "]: Trying to access an array using " + str(self.access_dims) + " dimensions when there are " + str(self.array_dims) + " dimensions"

class SymbolTableNotEmptyError(CompilationError):
   def __init__(self):
      pass
   def __str__(self):
      return "Semantic Error: The symbol table was not empty after checking the semantics"

class AstTypeError(CompilationError):
   def __init__(self):
      pass
   def __str__():
      return "Semantic Error: The type of the AST needs to be a Prog"

class LookupSymbolTypeError(CompilationError):
   def __init__(self, actual, expected):
      self.actual = actual
      self.expected = expected
   def __str__(self):
      return "Semantic Error: The type returned from the symbol table is " + str(self.actual) + " but " + str(self.expected) + " was expected"

