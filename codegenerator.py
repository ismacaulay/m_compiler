#
# module: codegenerator.py
#
import textwrap
from ir import *

class CodeGenerator():
   def __init__(self):
      self._newline = textwrap.dedent('''
         ''')
      self._conditional_label_num = 1
      self._case_label_num = 1
      self._current_level = 0
      self._nested_functions_code = []

   def generate(self, ir):
      # setup activation record
      code =  '   LOAD_R %sp' + self._newline 
      code += '   LOAD_R %sp' + self._newline 
      code += '   STORE_R %fp' + self._newline
      code += '   ALLOC ' + str(ir.num_local) + self._newline
      code += '   LOAD_I ' + str(ir.num_local + 1) + self._newline

      # alloc local arrays
      code += self._gencode_array_alloc(ir.num_local, ir.array_descs)

      # generate body code
      for stmt in ir.body_stmts:
         code += self._gencode_stmt(stmt)

      # load dealloc amount
      code += '   LOAD_R %fp' + self._newline
      code += '   LOAD_O ' + str(ir.num_local + 1) + self._newline
      code += '   LOAD_I 1' + self._newline
      code += '   APP ADD' + self._newline
      code += '   APP NEG' + self._newline
      code += '   ALLOC_S' + self._newline
      code += '   HALT' + self._newline

      # generate function code
      code += self._newline
      for func in ir.funcs:
         code += self._gencode_function(func)
         code += self._newline

      # generate function code for nested functions
      for func_code in self._nested_functions_code:
         code += func_code
         code += self._newline

      return code

   def _gencode_function(self, function):
      self._current_level += 1
      # setup activation record
      code = function.name + ':' + self._newline
      code += '   LOAD_R %sp' + self._newline
      code += '   STORE_R %fp' + self._newline
      code += '   ALLOC ' + str(function.num_local) + self._newline
      code += '   LOAD_I ' + str(function.num_local + 2) + self._newline

      code += self._gencode_array_alloc(function.num_local, function.array_descs)

      # generate statement code
      for stmt in function.body_stmts:
         code += self._gencode_stmt(stmt)

      # write return value
      code += '   LOAD_R %fp' + self._newline
      code += '   STORE_O ' + str((-(function.num_args + 3))) + self._newline
      # put return code pointer on top of return value
      code += '   LOAD_R %fp' + self._newline
      code += '   LOAD_O 0' + self._newline
      code += '   LOAD_R %fp' + self._newline
      code += '   STORE_O ' + str(-(function.num_args + 2)) + self._newline
      # deallocate local storage
      code += '   LOAD_R %fp' + self._newline
      code += '   LOAD_O ' + str(function.num_local + 1) + self._newline
      code += '   APP NEG' + self._newline
      code += '   ALLOC_S' + self._newline
      # restore callers fp
      code += '   STORE_R %fp' + self._newline
      # cleanup args
      code += '   ALLOC ' + str(-function.num_args) + self._newline
      # jump to return code ptr
      code += '   JUMP_S' + self._newline

      # generate code for the nested functions
      for f in function.funcs:
         self._nested_functions_code.append(self._gencode_function(f))

      self._current_level -= 1
      return code

#################################################
# Stmt code generators
#################################################
   def _gencode_stmt(self, stmt):
      if isinstance(stmt, I_Assign):
         return self._gencode_assign(stmt)
      elif isinstance(stmt, I_While):
         return self._gencode_while(stmt)
      elif isinstance(stmt, I_Cond):
         return self._gencode_cond(stmt)
      elif isinstance(stmt, I_Case):
         return self._gencode_case(stmt)
      elif isinstance(stmt, I_ReadBool):
         return self._gencode_read(stmt, "B")
      elif isinstance(stmt, I_PrintBool):
         return self._gencode_print(stmt, "B")
      elif isinstance(stmt, I_ReadInt):
         return self._gencode_read(stmt, "I")
      elif isinstance(stmt, I_PrintInt):
         return self._gencode_print(stmt, "I")
      elif isinstance(stmt, I_ReadFloat):
         return self._gencode_read(stmt, "F")
      elif isinstance(stmt, I_PrintFloat):
         return self._gencode_print(stmt, "F")
      elif isinstance(stmt, I_ReadChar):
         return self._gencode_read(stmt, "C")
      elif isinstance(stmt, I_PrintChar):
         return self._gencode_print(stmt, "C")
      elif isinstance(stmt, I_Return):
         return self._gencode_return(stmt)
      elif isinstance(stmt, I_Block):
         return self._gencode_block(stmt)

   def _gencode_assign(self, stmt):
      # generate the expression code
      code = self._gencode_expr(stmt.expr)

      # if there are array indicies
      if len(stmt.array_indicies) > 0:
         # generate the code to access the offset into the array
         code += self._gencode_array_offset(stmt.level, stmt.offset, stmt.array_indicies)
         # store the value
         code += '   STORE_OS' + self._newline
      else:
         # generate the code for the level
         code += self._gencode_level(stmt.level)
         # store the value
         code += '   STORE_O ' + str(stmt.offset) + self._newline
      return code

   def _gencode_while(self, stmt):
      cond_label_num = self._conditional_label_num
      self._conditional_label_num += 1

      code = 'while_stmt_' + str(cond_label_num) + ':' + self._newline
      # generate the expression code
      code += self._gencode_expr(stmt.expr)
      code += '   JUMP_C while_end_' + str(cond_label_num) + self._newline
      # generate the do stmt code
      code += self._gencode_stmt(stmt.stmt)
      code += '   JUMP while_stmt_' + str(cond_label_num) + self._newline
      code += 'while_end_' + str(cond_label_num) + ':' + self._newline
      return code

   def _gencode_cond(self, stmt):
      cond_label_num = self._conditional_label_num
      self._conditional_label_num += 1

      # generate the conditional expression code
      code = self._gencode_expr(stmt.expr)
      code += '   JUMP_C cond_stmt_' + str(cond_label_num) + self._newline
      # generate the then stmt code
      code += self._gencode_stmt(stmt.then_stmt)
      code += '   JUMP cond_end_' + str(cond_label_num) + self._newline
      code += 'cond_stmt_' + str(cond_label_num) + ':' + self._newline
      # generate the else stmt code
      code += self._gencode_stmt(stmt.else_stmt)
      code += 'cond_end_' + str(cond_label_num) + ':' + self._newline
      return code

   def _gencode_case(self, stmt):
      code = self._gencode_expr(stmt.expr)
      # set up an activation record for the case stmt
      code += '   LOAD_R %fp' + self._newline
      code += '   ALLOC 2' + self._newline
      code += '   LOAD_R %sp' + self._newline
      code += '   STORE_R %fp' + self._newline
      code += '   LOAD_R %fp' + self._newline
      code += '   LOAD_O -3' + self._newline
      code += '   LOAD_H' + self._newline
      code += '   JUMP_O' + self._newline

      # store the cases based on the constructor number.
      # this makes the case labels in the order of the constructor number
      cases = sorted(stmt.case_stmts, key=lambda c: c.cons_num)

      # store the case number for this case, this is necessary for nested cases
      case_label_num = self._case_label_num
      self._case_label_num += 1

      # generate the jump cases to jump to the correct case
      for i in range(0, len(cases)):
         code += '   JUMP case_' + str(case_label_num) + '_' + str(i) + self._newline

      # generate the case statment code
      for i in range(0, len(cases)):
         self._current_level += 1
         code += 'case_' + str(case_label_num) + '_' + str(i) + ':' + self._newline
         cs = cases[i]
         code += self._gencode_stmt(cs.stmt)
         # clean up the case activation record
         code += '   ALLOC ' + str(-(cs.num_args + 2)) + self._newline
         code += '   JUMP case_' + str(case_label_num) + '_return' + self._newline
         self._current_level -= 1

      # the return case
      code += 'case_' + str(case_label_num) + '_return:' + self._newline
      code += '   STORE_R %fp' + self._newline
      code += '   ALLOC -1' + self._newline
      return code

   def _gencode_read(self, stmt, read_type):
      code = '   READ_' + str(read_type) + self._newline

      # if there are array indicies in the read statement
      if len(stmt.array_indicies) > 0:
         # generate the code to get to the offset of the array
         code += self._gencode_array_offset(stmt.level, stmt.offset, stmt.array_indicies)
         code += '   STORE_OS' + self._newline
      else:
         # generate the code to get the correct level
         code += self._gencode_level(stmt.level)
         code += '   STORE_O ' + str(stmt.offset) + self._newline
      return code

   def _gencode_print(self, stmt, print_type):
      # generate the print expression code
      code = self._gencode_expr(stmt.expr)
      code += '   PRINT_' + str(print_type) + self._newline
      return code

   def _gencode_return(self, stmt):
      # generate the return expression code
      return self._gencode_expr(stmt.expr)

   def _gencode_block(self, stmt):
      self._current_level += 1

      # set up the activation record
      code =  '   LOAD_R %fp' + self._newline
      code += '   ALLOC 2' + self._newline
      code += '   LOAD_R %sp' + self._newline
      code += '   STORE_R %fp' + self._newline
      code += '   ALLOC ' + str(stmt.num_local) + self._newline
      code += '   LOAD_I ' + str(stmt.num_local + 3) + self._newline

      # generate the code to alloc the local arrays
      code += self._gencode_array_alloc(stmt.num_local, stmt.array_descs)

      # generate the body statments code
      for s in stmt.body_stmts:
         code += self._gencode_stmt(s)

      # clean up code
      code += '   LOAD_R %fp' + self._newline
      code += '   LOAD_O ' + str(stmt.num_local + 1) + self._newline
      code += '   APP NEG' + self._newline
      code += '   ALLOC_S' + self._newline
      code += '   STORE_R %fp' + self._newline

      # add the functions to the nested functions list so that they can
      # be generated at the end
      for f in stmt.funcs:
         self._nested_functions_code.append(self._gencode_function(f))

      self._current_level -= 1
      return code

#################################################
# Expr code generators
#################################################
   def _gencode_expr(self, expr):
      if isinstance(expr, I_IVal):
         return self._gencode_ival(expr)
      elif isinstance(expr, I_RVal):
         return self._gencode_rval(expr)
      elif isinstance(expr, I_BVal):
         return self._gencode_bval(expr)
      elif isinstance(expr, I_CVal):
         return self._gencode_cval(expr)
      elif isinstance(expr, I_Id):
         return self._gencode_id(expr)
      elif isinstance(expr, I_App):
         return self._gencode_app(expr)
      elif isinstance(expr, I_Ref):
         return self._gencode_ref(expr)
      elif isinstance(expr, I_Size):
         return self._gencode_size(expr)

   def _gencode_ival(self, expr):
      return '   LOAD_I ' + str(expr.value) + self._newline

   def _gencode_rval(self, expr):
      return '   LOAD_F ' + str(expr.value) + self._newline

   def _gencode_bval(self, expr):
      return '   LOAD_B ' + str(expr.value).lower() + self._newline

   def _gencode_cval(self, expr):
      return '   LOAD_C ' + str(expr.value) + self._newline

   def _gencode_id(self, expr):
      code = ''
      # if the id has array indicies
      if len(expr.array_indicies) > 0:
         # calculate the offset
         code += self._gencode_array_offset(expr.level, expr.offset, expr.array_indicies)
         code += '   LOAD_OS' + self._newline
      else:
         # calculate the level
         code += self._gencode_level(expr.level)
         code += '   LOAD_O ' + str(expr.offset) + self._newline
         
      return code

   def _gencode_app(self, expr):
      if isinstance(expr.op, I_Call):
         return self._gencode_function_call(expr)
      elif isinstance(expr.op, I_Cons):
         return self._gencode_cons(expr)
      elif isinstance(expr.op, I_Op):
         return self._gencode_operation(expr)

   def _gencode_ref(self, expr):
      code = self._gencode_level(expr.level)
      code += '   LOAD_O ' + str(expr.offset) + self._newline
      return code

   def _gencode_size(self, expr):
      code = self._gencode_level(expr.level)
      code += '   LOAD_O ' + str(expr.offset) + self._newline
      code += '   LOAD_O ' + str(expr.dimension) + self._newline
      return code

#################################################
# Operation code generators
#################################################
   def _gencode_function_call(self, expr):
      code = ''
      # generate the code for for the function call
      for e in expr.exprs:
         code += self._gencode_expr(e)
      code +=  '   ALLOC 1' + self._newline
      code += self._gencode_level(expr.op.level)
      code += '   LOAD_R %fp' + self._newline
      code += '   LOAD_R %cp' + self._newline
      code += '   JUMP ' + str(expr.op.label) + self._newline
      return code

   def _gencode_cons(self, expr):
      code = ''
      # generate the code for a constructor
      for e in expr.exprs:
         code += self._gencode_expr(e)

      code += '   LOAD_I ' + str(expr.op.cons_num) + self._newline
      code += '   STORE_H ' + str(expr.op.num_args + 1) + self._newline
      return code

   def _gencode_operation(self, expr):
      code = ''
      # generate the code for the operation
      for e in expr.exprs:
         code += self._gencode_expr(e)
      code += '   APP ' + self._gencode_op(expr.op) + self._newline
      return code
#################################################
# Helpers
#################################################
   def _gencode_level(self, level):
      # load the frame pointer
      code = '   LOAD_R %fp' + self._newline
      i = level
      # trace the frame pointer back until your at the right level
      while i < self._current_level:
         code += '   LOAD_O -2' + self._newline
         i += 1

      return code

   def _gencode_array_alloc(self, num_local, array_descs):
      code = ''
      for array_desc in array_descs:
         store_offset = True
         for e in array_desc.exprs:
            code += self._gencode_expr(e)
            # store a pointer to the first array dim into its variable
            if store_offset:
               code += '   LOAD_R %sp' + self._newline
               code += '   LOAD_R %fp' + self._newline
               code += '   STORE_O ' + str(array_desc.offset) + self._newline
               store_offset = False

         # compute number of array elements
         for i in range(0, len(array_desc.exprs)):
            # load the array dim value
            code += '   LOAD_R %fp' + self._newline
            code += '   LOAD_O ' + str(array_desc.offset) + self._newline
            code += '   LOAD_O ' + str(i) + self._newline
            # mul them if needed
            if i == 1:
               code += '   APP MUL' + self._newline
            elif i > 1:
               code += '   APP MUL' + self._newline

         # here the total number of array elements is ontop of the stack
         # load the dealloc value
         code += '   LOAD_R %fp' + self._newline
         code += '   LOAD_O ' + str(num_local + 1) + self._newline
         # load the number of dims
         code += '   LOAD_I ' + str(len(array_desc.exprs)) + self._newline
         # load the total number of elements
         code += '   LOAD_R %fp' + self._newline
         code += '   LOAD_O ' + str(array_desc.offset) + self._newline
         code += '   LOAD_O ' + str(len(array_desc.exprs)) + self._newline
         # add the total number of elements to number of dims
         code += '   APP ADD' + self._newline
         # add the result to the dealloc value
         code += '   APP ADD' + self._newline
         # update the dealloc value
         code += '   LOAD_R %fp' + self._newline
         code += '   STORE_O ' + str(num_local + 1) + self._newline
         # allocate the space
         code += '   ALLOC_S' + self._newline
      return code

   def _gencode_array_offset(self, level, offset, array_indicies):
      # load the variable
      code = self._gencode_level(level)
      code += '   LOAD_O ' + str(offset) + self._newline

      # compute offsets into array
      for i in range(0, len(array_indicies)):
         code += self._gencode_expr(array_indicies[i])
         # load the array dims
         for j in range(i+1, len(array_indicies)):
            code += self._gencode_level(level)
            code += '   LOAD_O ' + str(offset) + self._newline
            code += '   LOAD_O ' + str(j) + self._newline
         # mul everything together
         for j in range(i+1, len(array_indicies)):
            code += '   APP MUL' + self._newline
      # all all the offsets together
      for i in range(1, len(array_indicies)):
         code += '   APP ADD' + self._newline

      # add the number of dims to the offset value
      code += '   LOAD_I ' + str(len(array_indicies)) + self._newline
      code += '   APP ADD' + self._newline
      return code

   def _gencode_op(self, op):
      '''
      Returns the correct operation used in the AM stack machine
      '''
      op_str = str(op.op).upper()
      if 'LT' in op_str:
         if op_str.endswith('INT'):
            return 'LT'
         elif op_str.endswith('FLOAT'):
            return 'LT_F'
         elif op_str.endswith('CHAR'):
            return 'LT_C'
      elif 'LE' in op_str:
         if op_str.endswith('INT'):
            return 'LE'
         elif op_str.endswith('FLOAT'):
            return 'LE_F'
         elif op_str.endswith('CHAR'):
            return 'LE_C'
      elif 'GT' in op_str:
         if op_str.endswith('INT'):
            return 'GT'
         elif op_str.endswith('FLOAT'):
            return 'GT_F'
         elif op_str.endswith('CHAR'):
            return 'GT_C'
      elif 'GE' in op_str:
         if op_str.endswith('INT'):
            return 'GE'
         elif op_str.endswith('FLOAT'):
            return 'GE_F'
         elif op_str.endswith('CHAR'):
            return 'GE_C'
      elif 'EQ' in op_str:
         if op_str.endswith('INT'):
            return 'EQ'
         elif op_str.endswith('FLOAT'):
            return 'EQ_F'
         elif op_str.endswith('CHAR'):
            return 'EQ_C'
      elif 'ADD' in op_str:
         if op_str.endswith('INT'):
            return 'ADD'
         elif op_str.endswith('FLOAT'):
            return 'ADD_F'
      elif 'SUB' in op_str:
         if op_str.endswith('INT'):
            return 'SUB'
         elif op_str.endswith('FLOAT'):
            return 'SUB_F'
      elif 'DIV' in op_str:
         if op_str.endswith('INT'):
            return 'DIV'
         elif op_str.endswith('FLOAT'):
            return 'DIV_F'
      elif 'MUL' in op_str:
         if op_str.endswith('INT'):
            return 'MUL'
         elif op_str.endswith('FLOAT'):
            return 'MUL_F'
      elif 'NEG' in op_str:
         if op_str.endswith('INT'):
            return 'NEG'
         elif op_str.endswith('FLOAT'):
            return 'NEG_F'
      elif 'FLOOR' in op_str:
         return 'FLOOR'
      elif 'FLOAT' in op_str:
         return 'FLOAT'
      elif 'CEIL' in op_str:
         return 'CEIL'
      elif 'AND' in op_str:
         return 'AND'
      elif 'OR' in op_str:
         return 'OR'
      elif 'NOT' in op_str:
         return 'NOT'
