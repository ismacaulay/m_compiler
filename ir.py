#
# module: ir.py
#
#######################################
# I_Prog
#######################################
class I_Prog():
   '''
   I_Prog ([I_FBody], int, [I_ArrDesc], [I_Stmt])
   '''
   def __init__(self, funcs, num_local, array_descs, body_stmts):
      self.funcs = funcs
      self.num_local = num_local
      self.array_descs = array_descs
      self.body_stmts = body_stmts
   def __str__(self):
      funcs_str = "["
      if len(self.funcs) > 0:
         for f in self.funcs[:-1]:
            funcs_str += str(f) + ", "
         funcs_str += str(self.funcs[-1])
      funcs_str += "]"
      array_descs_str = "["
      if len(self.array_descs) > 0:
         for ad in self.array_descs[:-1]:
            array_descs_str += str(ad) + ", "
         array_descs_str += str(self.array_descs[-1])
      array_descs_str += "]"
      stmts_str = "["
      if len(self.body_stmts) > 0:
         for s in self.body_stmts[:-1]:
            stmts_str += str(s) + ", "
         stmts_str += str(self.body_stmts[-1])
      stmts_str += "]"
      return "I_Prog (" + funcs_str + ", " + str(self.num_local) + ", " + array_descs_str + ", " + stmts_str + ")"

#######################################
# I_FBody
#######################################
class I_Fun():
   '''
   I_Fun (string, [I_FBody], int, int, [I_ArrDesc], [I_Stmt])
   '''
   def __init__(self, name, funcs, num_local, num_args, array_descs, body_stmts):
      self.name = name
      self.funcs = funcs
      self.num_local = num_local
      self.num_args = num_args
      self.array_descs = array_descs
      self.body_stmts = body_stmts
   def __str__(self):
      funcs_str = "["
      if len(self.funcs) > 0:
         for f in self.funcs[:-1]:
            funcs_str += str(f) + ", "
         funcs_str += str(self.funcs[-1])
      funcs_str += "]"
      array_descs_str = "["
      if len(self.array_descs) > 0:
         for ad in self.array_descs[:-1]:
            array_descs_str += str(ad) + ", "
         array_descs_str += str(self.array_descs[-1])
      array_descs_str += "]"
      stmts_str = "["
      if len(self.body_stmts) > 0:
         for s in self.body_stmts[:-1]:
            stmts_str += str(s) + ", "
         stmts_str += str(self.body_stmts[-1])
      stmts_str += "]"
      return "I_Fun (" + str(self.name) + ", " + funcs_str + ", " + str(self.num_local) + ", " + str(self.num_args) + ", " + array_descs_str + ", " + stmts_str + ")"

#######################################
# I_Stmt
#######################################
class I_Assign():
   '''
   I_Assign (int, int, [I_Expr], I_Expr)
   '''
   def __init__(self, level, offset, array_indicies, expr):
      self.level = level
      self.offset = offset
      self.array_indicies = array_indicies
      self.expr = expr
   def __str__(self):
      array_indicies_str = "["
      if len(self.array_indicies) > 0:
         for i in self.array_indicies[:-1]:
            array_indicies_str += str(i) + ", "
         array_indicies_str += str(self.array_indicies[-1])
      array_indicies_str += "]"
      return "I_Assign (" + str(self.level) + ", " + str(self.offset) + ", " + array_indicies_str + ", " + str(self.expr) + ")"

class I_While():
   '''
   I_While (I_Expr, I_Stmt)
   '''
   def __init__(self, expr, stmt):
      self.expr = expr
      self.stmt = stmt
   def __str__(self):
      return "I_While (" + str(self.expr) + ", " + str(self.stmt) + ")"

class I_Cond():
   '''
   I_Cond (I_Expr, I_Stmt, I_Stmt)
   '''
   def __init__(self, expr, then_stmt, else_stmt):
      self.expr = expr
      self.then_stmt = then_stmt
      self.else_stmt = else_stmt
   def __str__(self):
      return "I_Cond (" + str(self.expr) + ", " + str(self.then_stmt) + ", " + str(self.else_stmt) + ")"

class I_Case():
   '''
   I_Case (I_Expr, [I_CaseStmt])
   '''
   def __init__(self, expr, case_stmts):
      self.expr = expr
      self.case_stmts = case_stmts
   def __str__(self):
      case_stmts_str = "["
      if len(self.case_stmts) > 0:
         for cs in self.case_stmts[:-1]:
            case_stmts_str += str(cs) + ", "
         case_stmts_str += str(self.case_stmts[-1])
      case_stmts_str += "]"
      return "I_Case (" + str(self.expr) + ", " + case_stmts_str + ")"

class I_CaseStmt():
   '''
   I_CaseStmt (int, int, I_Stmt)
   '''
   def __init__(self, cons_num, num_args, stmt):
      self.cons_num = cons_num
      self.num_args = num_args
      self.stmt = stmt
   def __str__(self):
      return "(" + str(self.cons_num) + ", " + str(self.num_args) + ", " + str(self.stmt) + ")"

class I_ReadBool():
   '''
   I_ReadBool (int, int, [I_Expr])
   '''
   def __init__(self, level, offset, array_indicies):
      self.level = level
      self.offset = offset
      self.array_indicies = array_indicies
   def __str__(self):
      array_indicies_str = "["
      if len(self.array_indicies) > 0:
         for i in self.array_indicies[:-1]:
            array_indicies_str += str(i) + ", "
         array_indicies_str += str(self.array_indicies[-1])
      array_indicies_str += "]"
      return "I_Read_Bool (" + str(self.level) + ", " + str(self.offset) + ", " + array_indicies_str + ")"

class I_PrintBool():
   '''
   I_PrintBool I_Expr
   '''
   def __init__(self, expr):
      self.expr = expr
   def __str__(self):
      return "I_Print_Bool (" + str(self.expr) + ")"

class I_ReadInt():
   '''
   I_ReadInt (int, int, [I_Expr])
   '''
   def __init__(self, level, offset, array_indicies):
      self.level = level
      self.offset = offset
      self.array_indicies = array_indicies
   def __str__(self):
      array_indicies_str = "["
      if len(self.array_indicies) > 0:
         for i in self.array_indicies[:-1]:
            array_indicies_str += str(i) + ", "
         array_indicies_str += str(self.array_indicies[-1])
      array_indicies_str += "]"
      return "I_Read_Int (" + str(self.level) + ", " + str(self.offset) + ", " + array_indicies_str + ")"

class I_PrintInt():
   '''
   I_PrintInt I_Expr
   '''
   def __init__(self, expr):
      self.expr = expr
   def __str__(self):
      return "I_Print_Int (" + str(self.expr) + ")"

class I_ReadFloat():
   '''
   I_ReadFloat (int, int, [I_Expr])
   '''
   def __init__(self, level, offset, array_indicies):
      self.level = level
      self.offset = offset
      self.array_indicies = array_indicies
   def __str__(self):
      array_indicies_str = "["
      if len(self.array_indicies) > 0:
         for i in self.array_indicies[:-1]:
            array_indicies_str += str(i) + ", "
         array_indicies_str += str(self.array_indicies[-1])
      array_indicies_str += "]"
      return "I_Read_Float (" + str(self.level) + ", " + str(self.offset) + ", " + array_indicies_str + ")"

class I_PrintFloat():
   '''
   I_PrintFloat I_Expr
   '''
   def __init__(self, expr):
      self.expr = expr
   def __str__(self):
      return "I_Print_Float (" + str(self.expr) + ")"

class I_ReadChar():
   '''
   I_ReadChar (int, int, [I_Expr])
   '''
   def __init__(self, level, offset, array_indicies):
      self.level = level
      self.offset = offset
      self.array_indicies = array_indicies
   def __str__(self):
      array_indicies_str = "["
      if len(self.array_indicies) > 0:
         for i in self.array_indicies[:-1]:
            array_indicies_str += str(i) + ", "
         array_indicies_str += str(self.array_indicies[-1])
      array_indicies_str += "]"
      return "I_Read_Char (" + str(self.level) + ", " + str(self.offset) + ", " + array_indicies_str + ")"

class I_PrintChar():
   '''
   I_PrintChar I_Expr
   '''
   def __init__(self, expr):
      self.expr = expr
   def __str__(self):
      return "I_Print_Char (" + str(self.expr) + ")"

class I_Return():
   '''
   I_Return I_Expr
   '''
   def __init__(self, expr):
      self.expr = expr
   def __str__(self):
      return "I_Return (" + str(self.expr) + ")"

class I_Block():
   '''
   I_Block ([I_FBody], int, [I_ArrDesc], [I_Stmt])
   '''
   def __init__(self, funcs, num_local, array_descs, body_stmts):
      self.funcs = funcs
      self.num_local = num_local
      self.array_descs = array_descs
      self.body_stmts = body_stmts
   def __str__(self):
      funcs_str = "["
      if len(self.funcs) > 0:
         for f in self.funcs[:-1]:
            funcs_str += str(f) + ", "
         funcs_str += str(self.funcs[-1])
      funcs_str += "]"
      array_descs_str = "["
      if len(self.array_descs) > 0:
         for ad in self.array_descs:
            array_descs_str += str(ad) + ", "
         array_descs_str += str(self.array_descs[-1])
      array_descs_str += "]"
      stmts_str = "["
      if len(self.body_stmts) > 0:
         for s in self.body_stmts[:-1]:
            stmts_str += str(s) + ", "
         stmts_str += str(self.body_stmts[-1])
      stmts_str += "]"
      return "I_Block (" + funcs_str + ", " + str(self.num_local) + ", " + array_descs_str + ", " + stmts_str + ")"

class I_ArrDesc():
   '''
   I_ArrDesc (int, [I_Expr])
   '''
   def __init__(self, offset, exprs):
      self.offset = offset
      self.exprs = exprs
   def __str__(self):
      exprs_str = "["
      if len(self.exprs) > 0:
         for e in self.exprs[:-1]:
            exprs_str += str(e) + ", "
         exprs_str += str(self.exprs[-1])
      exprs_str += "]"
      return "(" + str(self.offset) + ", " + exprs_str + ")"

#######################################
# I_Expr
#######################################
class I_IVal():
   '''
   I_IVal int
   '''
   def __init__(self, value):
      self.value = value
   def __str__(self):
      return "I_IVal " + str(self.value)

class I_RVal():
   '''
   I_RVal float
   '''
   def __init__(self, value):
      self.value = value
   def __str__(self):
      return "I_RVal " + str(self.value)

class I_BVal():
   '''
   I_BVal bool
   '''
   def __init__(self, value):
      self.value = value
   def __str__(self):
      return "I_BVal " + str(self.value)

class I_CVal():
   '''
   I_CVal char
   '''
   def __init__(self, value):
      self.value = value
   def __str__(self):
      return "I_CVal " + str(self.value)

class I_Id():
   '''
   I_Id (int, int, [I_Expr])
   '''
   def __init__(self, level, offset, array_indicies):
      self.level = level
      self.offset = offset
      self.array_indicies = array_indicies
   def __str__(self):
      array_indicies_str = "["
      if len(self.array_indicies) > 0:
         for i in self.array_indicies[:-1]:
            array_indicies_str += str(i) + ", "
         array_indicies_str += str(self.array_indicies[-1])
      array_indicies_str += "]"
      return "I_Id (" + str(self.level) + ", " + str(self.offset) + ", " + array_indicies_str + ")"

class I_App():
   '''
   I_App (I_Opn, [I_Expr])
   '''
   def __init__(self, op, exprs):
      self.op = op
      self.exprs = exprs
   def __str__(self):
      exprs_str = "["
      if len(self.exprs) > 0:
         for e in self.exprs[:-1]:
            exprs_str += str(e) + ", "
         exprs_str += str(self.exprs[-1])
      exprs_str += "]"
      return "I_App (" + str(self.op) + ", " + exprs_str + ")"

class I_Ref():
   '''
   I_Ref (int, int)
   '''
   def __init__(self, level, offset):
      self.level = level
      self.offset = offset
   def __str__(self):
      return "I_Ref (" + str(self.level) + ", " + str(self.offset) + ")"

class I_Size():
   '''
   I_Size (int, int, int)
   '''
   def __init__(self, level, offset, dimension):
      self.level = level
      self.offset = offset
      self.dimension = dimension
   def __str__(self):
      return "I_Size (" + str(self.level) + ", " + str(self.offset) + ", " + str(self.dimension) + ")"

#######################################
# I_Opn
#######################################
class I_Call():
   '''
   I_Call (string, int)
   '''
   def __init__(self, label, level):
      self.label = label
      self.level = level
   def __str__(self):
      return "I_Call (" + str(self.label) + ", " + str(self.level) + ")"

class I_Cons():
   '''
   I_Cons (int, int)
   '''
   def __init__(self, cons_num, num_args):
      self.cons_num = cons_num
      self.num_args = num_args
   def __str__(self):
      return "I_Cons (" + str(self.cons_num) + ", " + str(self.num_args) + ")"

class I_Op():
   '''
   I_Op
   '''
   def __init__(self, op):
      self.op = op
   def __str__(self):
      return str(self.op)
